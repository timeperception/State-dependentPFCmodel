 # -*- coding: utf-8 -*-
"""
Created on Wed Oct 02 10:46:01 2019

@author: Pira
"""

import scipy.io
import scipy
import pylab as pl
import matplotlib

import numpy as np
import numpy.random as rd
from tqdm import tqdm
import ray
import os
from datetime import date

num_cpus = 5
if 'ip_head' in os.environ:
    ray.init(address=os.environ["ip_head"])
else:
    ray.init(num_cpus=num_cpus)

@ray.remote
def _get_liquid_states(spike_times, times, tau, t_window=None):
    def _windowed_events(events, window_times, window_size):
        """
        Generate subsets of events which belong to given time windows.

        Assumptions:
        * events are sorted
        * window_times are sorted

        :param events: one-dimensional, sorted list of event times
        :param window_times: the upper (exclusive) boundaries of time windows
        :param window_size: the size of the windows
        :return: generator yielding (window_time, window_events)
        """
        for window_time in reversed(window_times):
            #print(window_time, events)
            events = events[events < window_time]
            yield window_time, events[events > window_time - window_size]
    
    n_neurons = np.size(spike_times, 0)
    n_times = np.size(times, 0)
    states = np.zeros((n_times, n_neurons))
    if t_window is None:
        t_window = 5 * tau
    for n, spt in enumerate(spike_times):
        # TODO state order is reversed, as windowed_events are provided in reversed order
        for i, (t, window_spikes) in enumerate(_windowed_events(np.array(spt), times, t_window)):
            #print(t-window_spikes)
            states[n_times - i - 1, n] = np.sum(np.exp(-(t - window_spikes) / tau))
    return states

'''
def windowed_events(events, window_times, window_size):
    """
    Generate subsets of events which belong to given time windows.

    Assumptions:
    * events are sorted
    * window_times are sorted

    :param events: one-dimensional, sorted list of event times
    :param window_times: the upper (exclusive) boundaries of time windows
    :param window_size: the size of the windows
    :return: generator yielding (window_time, window_events)
    """
    for window_time in reversed(window_times):
        #print(window_time, events)
        events = events[events < window_time]
        yield window_time, events[events > window_time - window_size]
'''
      
def compute_readout_weights(states, targets, reg_fact=0):
    """
    Train readout with linear regression
    :param states: numpy array with states[i, j], the state of neuron j in example i
    :param targets: numpy array with targets[i], while target i corresponds to example i
    :param reg_fact: regularization factor; 0 results in no regularization
    :return: numpy array with weights[j]
    """
    if reg_fact == 0:
        w, residuals, rank, sing = scipy.linalg.lstsq(states, targets, cond=1e-3)
    else:
        w = np.dot(np.dot(np.linalg.inv(reg_fact * np.eye(np.size(states, 1)) + np.dot(states.T, states)), states.T), targets)
    return w, residuals, rank, sing

def compute_prediction(states, readout_weights):
    return np.dot(states, readout_weights)
    


def load_spikes_from_sender(intervals_origin, trial_all, folder, spiketrains, factor, factor_layer5, fac_GABAB, fac_GABAA, percent_flag, percent, syn_ex_fac, syn_in_fac, syn_fac, GABAB_adj_factor, weight_poisson, use_tqdm=False, syn_weight_param = False, syn_factor=False, GABAB_adj_factor_value=True, weight_poisson_value=True,layer5exc=False ):
    sender_all = [[]for i in trial_all]
    times_all = [[]for i in trial_all]
    if use_tqdm:
        iterate_over = tqdm(trial_all)
    else:
        iterate_over = trial_all
    for trial in iterate_over:
        for element in np.arange(0, len(intervals_origin)):
            if percent_flag==False:
                sender = np.load(folder +str(trial)+str(intervals_origin[element])+ str(factor)+ str(fac_GABAB)+str(fac_GABAA)+'sender_spike.npy', allow_pickle=True)
                times = np.load(folder +str(trial)+str(intervals_origin[element])+ str(factor)+ str(fac_GABAB)+str(fac_GABAA)+'times_spike.npy', allow_pickle=True)
                
            else:
                if syn_weight_param == True:
                    
                    if syn_factor ==True: 
                        if GABAB_adj_factor_value == True:
                            if weight_poisson_value ==True:
                                if layer5exc==True: 
                                    
                                    sender = np.load(folder +str(trial)+str(intervals_origin[element])+ str(factor)+ str(factor_layer5) + str(fac_GABAB)+str(fac_GABAA)+str(percent)+str(syn_ex_fac) +str(syn_in_fac) + str(syn_fac) +str(GABAB_adj_factor) +  'sender_spike.npy', allow_pickle=True)
                                    
                                    times  = np.load(folder +str(trial)+str(intervals_origin[element])+ str(factor)+ str(factor_layer5) + str(fac_GABAB)+str(fac_GABAA)+str(percent)+str(syn_ex_fac) +str(syn_in_fac) + str(syn_fac) +str(GABAB_adj_factor) +  'times_spike.npy', allow_pickle=True)
                                    
                                else:
                                    sender = np.load(folder +str(trial)+str(intervals_origin[element])+ str(factor)+  str(fac_GABAB)+str(fac_GABAA)+str(percent)+str(syn_ex_fac) +str(syn_in_fac) + str(syn_fac) +str(GABAB_adj_factor) +  'sender_spike.npy', allow_pickle=True)
                                    
                                    times  = np.load(folder +str(trial)+str(intervals_origin[element])+ str(factor)+  str(fac_GABAB)+str(fac_GABAA)+str(percent)+str(syn_ex_fac) +str(syn_in_fac) + str(syn_fac) +str(GABAB_adj_factor) +  'times_spike.npy', allow_pickle=True)

                            else:
                                sender = np.load(folder +str(trial)+str(intervals_origin[element])+ str(factor)+  str(fac_GABAB)+str(fac_GABAA)+str(percent)+str(syn_ex_fac) +str(syn_in_fac) + str(syn_fac) +str(GABAB_adj_factor) +  'sender_spike.npy', allow_pickle=True)
                                times  = np.load(folder +str(trial)+str(intervals_origin[element])+ str(factor)+  str(fac_GABAB)+str(fac_GABAA)+str(percent)+str(syn_ex_fac) +str(syn_in_fac) + str(syn_fac) +str(GABAB_adj_factor) +  'times_spike.npy', allow_pickle=True)
                                    
                        else:
                            sender = np.load(folder +str(trial)+str(intervals_origin[element])+ str(factor)+  str(fac_GABAB)+str(fac_GABAA)+str(percent)+str(syn_ex_fac) +str(syn_in_fac) + str(syn_fac) +  'sender_spike.npy', allow_pickle=True)
                            times  = np.load(folder +str(trial)+str(intervals_origin[element])+ str(factor)+  str(fac_GABAB)+str(fac_GABAA)+str(percent)+str(syn_ex_fac) +str(syn_in_fac) + str(syn_fac) +  'times_spike.npy', allow_pickle=True)
                    else:
                        sender = np.load(folder +str(trial)+str(intervals_origin[element])+ str(factor)+  str(fac_GABAB)+str(fac_GABAA)+str(percent)+str(syn_ex_fac) +str(syn_in_fac) +  'sender_spike.npy', allow_pickle=True)
                        times  = np.load(folder +str(trial)+str(intervals_origin[element])+ str(factor)+  str(fac_GABAB)+str(fac_GABAA)+str(percent)+str(syn_ex_fac) +str(syn_in_fac) +  'times_spike.npy', allow_pickle=True)
                        
                else:
                    sender = np.load(folder +str(trial)+str(intervals_origin[element])+ str(factor)+  str(fac_GABAB)+str(fac_GABAA)+str(percent) +  'sender_spike.npy', allow_pickle=True)
                    times  = np.load(folder +str(trial)+str(intervals_origin[element])+ str(factor)+  str(fac_GABAB)+str(fac_GABAA)+str(percent)+  'times_spike.npy', allow_pickle=True)

            spikes = [[]for m in np.arange(0,1003)]
            
            #print(spiketrains_python)
            #value = 0
            for k in np.arange(3,1001+2):
                a = np.where(sender == (k))
                spikes[int(k)-3].append(times[np.array(a)])
                #value+=1
            #value = 105
            #for l in np.arange(955, 1001):
            #    spikes[value].append(spiketrains_python[l][0])
            #    value +=1
            spiketrains[element].append(spikes)
            #sender_all[trial].append(sender)
            #times_all[trial].append(times)
    return spiketrains , sender_all, times_all




def states_1(spiketrains, intervals_origin, trials, starttime,tau, window, folder):
    readout_endtimes = intervals_origin + 1500 + starttime
    print(len(spiketrains))
    # states = [[]for i in np.arange((len(intervals_origin))*trials)]
    num_neurons = len(spiketrains[0][0])
          
    states = [
        _get_liquid_states.remote(spiketrains[k][j],readout_endtimes, tau, window)
        for k in np.arange(0,len(intervals_origin))
        for j in np.arange(0,trials)
    ]
    states = ray.get(states)
    states = np.array(states).reshape((len(intervals_origin), trials, len(readout_endtimes), num_neurons))
    np.save(folder +'states_'+str(window)+str(starttime)+'.npy', states)
    return states



def test_states_1(folder, intervals, trials_test, spiketrains_testset, starttime, tau, window):
    readout_times_test = intervals + 1500 + starttime
    
    num_neurons = len(spiketrains_testset[0][0])
    
    '''
    states_test = np.empty((len(intervals), trials_test, num_neurons))
    for k in tqdm(np.arange(0,len(intervals))):
        for j in np.arange(trials_test):
            states_test[k, j] = _get_liquid_states(spiketrains_testset[k][j], [readout_times_test[k]], tau, window)
    '''
    
    states_test = [
        _get_liquid_states.remote(spiketrains_testset[k][j],[readout_times_test[k]], tau, window)
        for k in np.arange(0,len(intervals))
        for j in np.arange(0,trials_test)
    ]
    # print('Waiting for _get_liquid_states ray jobs to complete...')
    # states_test = [ray.get(state) for state in tqdm(states_test)]
    states_test = ray.get(states_test)
    states_test = np.array(states_test).reshape((len(intervals), trials_test, num_neurons))
    
    np.save(folder + 'states_test' + str(window) + str(starttime) + '.npy', states_test)
    np.save(folder + 'intervals' + str(window) + str(starttime) + '.npy', intervals)
    return states_test


