# -*- coding: utf-8 -*-
"""
Created on Wed Mar 04 09:46:47 2020

@author: Pira
"""

import os
import itertools
import numpy as np
import pandas as pd
import socket
#import functions_simulation as funcsim
hostname = socket.gethostname()

if "pira" in hostname.lower():
    base_folder = "/Users/pirathitharavichandran/Documents/pot_cond_of_special_simulations/"
    default_num_cpus = 12
elif "uc" in hostname.lower(): # bwcluster
    base_folder = "/pfs/work7/workspace/scratch/hd_ta181-state_space/results_mai_2022/"
    #base_folder = funcsim.get_current_basefolder()
else:
    default_num_cpus=20
    base_folder = '/zi-flstorage/Pirathitha.Ravichandran/'

############################## train ############################################

for i in np.arange(1, 2, 1):
    exp_name = 'Original_train'
    exp_folder = os.path.join(base_folder, exp_name)

    if not os.path.exists(exp_folder):
        os.mkdir(exp_folder)

    params_single = {
        'select': np.array([220.0]),
        'firing_poisson':np.array([1.0]),  
        'fac_GABAA': np.array([0.3]), 
        'percent' : np.array([0.0]),
    }

    param_keys = list(params_single.keys())
    param_values = list(params_single.values())
    param_combinations = list(itertools.product(*param_values))
    params_comb = dict()
    
    for k, key in enumerate(param_keys):
        params_comb[key] = []
        for l in range(len(param_combinations)):
            params_comb[key].append(param_combinations[l][k])

    params_comb['path'] = []
    for l in range(len(param_combinations)):
        cur_name = []
        for key in param_keys[:]:
            cur_name.append(key + '_' + str(params_comb[key][l]))
        cur_folder = '_'.join(cur_name)
        cur_path = os.path.join(exp_folder, cur_folder)
        params_comb['path'].append(cur_path)
        if not os.path.exists(cur_path):
            os.mkdir(cur_path)

    param_dframe = pd.DataFrame(data=params_comb)
    param_dframe.iloc[0, :].to_dict()
    dframe_path = os.path.join(exp_folder, exp_name + '_param_dframe.pkl')
    param_dframe.to_pickle(dframe_path)
    test_param_dframe = pd.read_pickle(dframe_path)
