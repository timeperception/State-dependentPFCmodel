# NoiseDistr : Not used by the moment
# ******************************************************
# This script will load the input data needed to run
# the PFC_ultimate_II model which is created by the
# matlab code.
#
# Each variable should be given in an independent .mat
# file. Parameters:
#
# CtrPar : [Starting time, Stopping time, time step, ViewList neurons, record spike times?]  (Config2)
# NeuPar : [C_m, g_L, E_L, sf, V_up, tcw, a, b, V_r, V_th, I, v_dep]  (c code 602-)
# NPList : obsolet
# STypPar : AMPA, GABA, NMDA # 691 IDNet.c
# SynPar : (Type, 3xPlasticity, weight, delay, proba fail)xsynapses
# SPMtx : Mirar papel
# EvtMtx : Each column defines an event (current discharge to neurons). must have as many rows as neurons in the model
# EvtTimes : Each column corresponds to an event. 1) beginning time 2) ending time
# ViewList : Id of neurons we want to look at in detail
# InpSTtrains : Each row is a neuron (feeding the system). Values are spike times
# NoiseDistr : Not used by the moment
# V0 : Initial conditions (V0, w0)
# UniqueNum : ID for temporary files (maybe not needed)dwasdas
# NeuronGroupsSaveArray : Another ViewList

# Originally written by Santiago Moreno Jaureguizar and adapted by Pirathitha Ravichandran-Schmidt

# ******************************************************

import scipy.io
import numpy as np

import socket
import yaml
import os
from types import SimpleNamespace

def get_params(folder):
    #######################################################
    # Loading parameters from matlab part (in folder)
    #######################################################
    # IMPROVE: maybe it's faster to load from an appropriate text file, cause loadmat makes weird things with matrix and arrays
    # syn_weights_alltoall = np.load(folder +'/syn_weights_alltoall.npy')
    CtrPar = scipy.io.loadmat(folder+'/CtrPar')
    NeuPar = scipy.io.loadmat(folder+'/NeuPar')
    NPList = scipy.io.loadmat(folder+'/NPList')
    STypPar = scipy.io.loadmat(folder+'/STyPar')
    SimPar = scipy.io.loadmat(folder+'/SimPar')
    SynPar = scipy.io.loadmat(folder+'/Synpar')
    SPMtx = scipy.io.loadmat(folder+'/SPMtx')
    EvtMtx = scipy.io.loadmat(folder+'/EvtMtx')
    EvtTimes = scipy.io.loadmat(folder+'/EvtTimes')
    ViewList = scipy.io.loadmat(folder+'/ViewList')

    #InpSTtrains = scipy.io.loadmat(folder+'/InpSTtrains')
    #NoiseDistr = scipy.io.loadmat(folder+'/NoiseDistr')
    V0 = scipy.io.loadmat(folder+'/V0')
    #dV = scipy.io.loadmat(folder+'/dV')
    #print dV
    #T_mat = scipy.io.loadmat(folder+'/T')
    #V_mat = scipy.io.loadmat(folder+'/V')

    # UniqueNum = scipy.io.loadmat(folder+'/UniqueNum')
    # NeuronGroupsSaveArray = scipy.io.loadmat(folder+'/NeuronGroupsSaveArray')

    CtrPar = CtrPar['CtrPar'][0].tolist()
    if len(CtrPar)!=5:
        print("CtrPar size should be 5. It isn't")
        ALL_RIGHT = False
    print(CtrPar)
    # NeuPar

    NeuPar = NeuPar['NeuPar'].tolist()
    if type(NeuPar) == list:
        NeuParAux = []
        j = 0
        j_stop = len(NeuPar[0])
        while j<j_stop:
            i = 0
            NeuAux = []
            while i<12:
                NeuAux.append(NeuPar[i][j])
                i=i+1
            NeuParAux.append(NeuAux)
            j = j+1
        NeuPar = NeuParAux # A list of lists: parameters sets, one per neuron.

    N_neurons = len(NeuPar)

    # SynPar

    SynPar = SynPar['SynPar']
    SynParAux = []
    j = 0
    j_stop = len(SynPar[0])
    while j<j_stop:
        i = 0
        SynAux = []
        while i<7:
            SynAux.append(SynPar[i][j])
            i=i+1
        SynParAux.append(SynAux)
        j = j+1
    SynParAux = np.array(SynParAux)
    assert np.all(SynParAux == SynPar.T)
    SynPar = np.ascontiguousarray(SynPar.T) # A list of lists: parameters sets, one per neuron.

    # STypPar:

    STypPar = STypPar['STypPar']
    ampa = []
    gaba = []
    nmda = []
    for t in STypPar:
        ampa.append(t[0])
        gaba.append(t[1])
        nmda.append(t[2])
    STypPar = {'AMPA':ampa,'GABA':gaba, 'NMDA':nmda}

    # SPMtx

    SPMtx =  SPMtx['SPMtx']
    SPMtx1 = []
    SPMtx2 = []
    for i in SPMtx:
        for j in i:
            SPMtx1.append(j[0])
            SPMtx2.append(j[1])
    SPMtx = [SPMtx1,SPMtx2] # Each matrix is read line by line

    # ViewList

    ViewList = ViewList['ViewList']

    # InpSTtrains

    #InpSTtrains = InpSTtrains['InpSTtrains'].tolist()

    # V0

    V0 = V0['V0'] # [[V0],[w0]]
    #dV = dV['dV'].tolist() # [[V0],[w0]]
    #T_mat = T_mat['T'].tolist() # [[V0],[w0]]

    # EvtMtx

    EvtMtx = EvtMtx['EvtMtx']
    if len(EvtMtx) > 0:
        EvtMtxAux = []
        j = 0
        j_stop = len(EvtMtx[0])
        while j<j_stop:
            i = 0
            EvtAux = []
            while i<N_neurons:
                EvtAux.append(EvtMtx[i][j])
                i=i+1
            EvtMtxAux.append(EvtAux)
            j = j+1
        EvtMtx = np.array(EvtMtxAux)

        # EvtTimes
    '''
        EvtTimes = EvtTimes['EvtTimes'].tolist()
        EvtTimesAux = []
        j = 0
        j_stop = len(EvtTimes[0])
        while j<j_stop:
            i = 0
            EvtAux = []
            while i<2:
                if EvtTimes[i][j]==0.005:
                    EvtTimes[i][j]=0.0
                EvtAux.append(EvtTimes[i][j])
                i=i+1
            EvtTimesAux.append(EvtAux)
            j = j+1
        EvtTimes = EvtTimesAux
    '''
    #NeuPar = NeuPar['NeuPar'].tolist()
    params2return = ('CtrPar', 'EvtMtx', 'V0', 'ViewList', 'SPMtx', 'STypPar', 'SynPar', 'NeuPar', 'N_neurons', 'SimPar')

    params2return =  {k:v for k, v in locals().items() if k in params2return}
    print('Params are loaded')

    return SimpleNamespace(**params2return)

