import yaml
import io
import os
import pandas as pd
import random
import numpy as np
import socket
import os
hostname = socket.gethostname()

############################## train ############################################
for i in np.arange(1, 2, 1):

	base_folder = "/Users/pirathitharavichandran/Documents/pot_cond_of_special_simulations/"
	default_num_cpus = 15
	folder = "Parameter/PFC_250_200_1000N_S1_s_1_1_1_1_p_1_1_1_1_1_1_3000ms_all_22-03-2021"


	gsearch_exp = 'Original_train'  # +str(i)

	dframe_path = os.path.join(base_folder, gsearch_exp)

	# Define data
	params = {
		'params_folder' : folder,
		'interval_start': 50,
		'interval_end'  : 775,
		'interval_step' :  25,

		'trial_start': 0,
		'trial_end':   1, 
		'trial_step':  1,

		'fac_GABAB' : 0.3,
		'weight_poisson':  [0.5],

		'GABAB_adj_factor_start': 1.4,
		'GABAB_adj_factor_end':  1.45,
		'GABAB_adj_factor_step': 0.1, 


		'syn_factor_start' :1.0,
		'syn_factor_end': 1.05,
		'syn_factor_step': 0.3,

		'syn_ex_fac_start' : 1.0,
		'syn_ex_fac_end': 1.05,
		'syn_ex_fac_step': 0.3,

		'syn_in_fac_start' : 1.0,
		'syn_in_fac_end': 1.05,
		'syn_in_fac_step': 0.1,
		# adj_factor
		'factor_start' : 1.0,
		'factor_end': 1.05,
		'factor_step': 0.5,

		'adj_layer5_start': 1.0,
		'adj_layer5_end': 1.05,
		'adj_layer5_step':0.5,

		'sim_time' : 2500.0,
		'fac_NMDA': 1.0,
        
		'inhibi_factor': 1.0,


		##############################
		'num_poisson' : 10,
		'number_input' : 1, ## 1 for origin, and 1000Hz cases before agusut 2021
		##############################
		### achtung: im code simulation_run connectivity ändern!!!! 
		# flags.
		'STP' : True,
		'weights_within_pools': False,
		'layer23exc_flag': False,
		'layer5exc_flag': False,
		'perturbation_flag':False,
		'perturbation_currents': [0.0], # , 0.017, 0.019, 0.02]
        #### not used: 
		'E_inh_b_factor' : float(-90), 
		'gmax_in_factor' :  5.0,   #origin: 5.0
		'tau_ingabab_factor' :float(100), #origin: #float(100)
		'tau_offgabab_factor' :float(200) #origin:  float(200)
	}
    

	# out_dir = '/Users/pirathitharavichandran/tmp'
	out_path = os.path.join(dframe_path, 'params.yaml')
	# np.save(dframe_path + '/list_intervals.npy', list_intervals)
	# Write YAML file
	with io.open(out_path, 'w', encoding='utf8') as outfile:
		yaml.dump(params, outfile)
