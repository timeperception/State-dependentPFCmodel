#!/bin/bash

# This script installs MyModule as nest module. It requires nest 2.18.0, install using:
# https://nest-simulator.readthedocs.io/en/v2.18.0/installation/linux_install.html#standard-installation

# Installs into custom nest_modules directory. Make sure to add the following lines to your .bashrc:
#  export NEST_MODULE_PATH=/home/hd/hd_hd/hd_ta181/nest_modules/lib64/nest:$NEST_MODULE_PATH
#  export SLI_PATH=/home/hd/hd_hd/hd_ta181/nest_modules/share/nest/sli:$SLI_PATH

# Under mac: add the following two lines to .bashrc/.zshrc
# export NEST_MODULE_PATH=$HOME/nest_modules/lib:$NEST_MODULE_PATH
# export SLI_PATH=$HOME/nest_modules/share/sli:$SLI_PATH


mkdir -p mmb
cd mmb
#if hostname=="uc2n994.localdomain":
####for bwcluster: add this line
#NEST_INSTALL_DIR="/opt/bwhpc/common/bio/nest/2.18.0"
###additionally change in /home/hd/XX/XX/github/MyModule_Nest218/mmb/CMakeFiles/mymodule_lib.dir and in mymodule_module.dir
###link.txt the command lib64/libltdl.so tooo --> /opt/bwhpc/common/bio/nest/2.18.0/lib64/libltdl.so


nest_modules_installdir="$HOME"  #/nest_modules"

#mkdir -p ${nest_modules_installdir}
### this line above is not necessary because it creates a new folder and we want to save in the same folder as nest is installes

cmake -Dwith-nest=${NEST_INSTALL_DIR}/bin/nest-config ../MyModule ###### -DCMAKE_INSTALL_PREFIX="${nest_modules_installdir}" ../MyModule

####### DO NOT FOrget to include the two lines into bashrc : 
# export NEST_MODULE_PATH=/home/XX/lib:$NEST_MODULE_PATH
# export SLI_PATH=/home/XX/share/sli:$SLI_PATH 

make
make install

cd ../MyModule
##make clean

cd ..


###################################ACHTUNG'#############################
##### if g++-9: error: unrecognized command line option '-stdlib=libc++' error occurs, then change
##### in mmb/CMakeFiles/3.17.1/CMakeCXXCompiler.cmake line 1 :
##### from set(CMAKE_CXX_COMPILER "/usr/local/bin/g++-9") to : set(CMAKE_CXX_COMPILER "/usr/bin/g++")
