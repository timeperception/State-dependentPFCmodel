#!/bin/zsh

# Instruction from: 
# https://nest-simulator.readthedocs.io/en/v2.18.0/installation/linux_install.html#standard-installation

download_dir="$HOME/downloads"
install_dir="$HOME/nest_2.18.0"

if [ ! -d ${download_dir} ]
then
  mkdir -p ${download_dir}

if [ ! -d ${install_dir} ]
then
  mkdir -p ${install_dir}
fi

cd ${download_dir}


brew install gcc cmake gsl open-mpi libtool

conda install numpy scipy matplotlib ipython cython nose

# download tar ball
wget https://github.com/nest/nest-simulator/archive/v2.18.0.tar.gz

# untar
tar -xzvf v2.18.0.tar.gz

rm v2.18.0.tar.gz

# Create build directory
mkdir nest-simulator-2.18.0-build
cd nest-simulator-2.18.0-build

cmake -DCMAKE_INSTALL_PREFIX:PATH="${install_dir}" -DCMAKE_C_COMPILER=gcc-9 -DCMAKE_CXX_COMPILER=g++-9 "${download_dir}/nest-simulator-2.18.0"
# Compile and install nest
make
make install
make installcheck

# Add variables to bashrc
echo "source ${install_dir}/bin/nest_vars.sh" >> $HOME/.zshrc
source $HOME/.zshrc


