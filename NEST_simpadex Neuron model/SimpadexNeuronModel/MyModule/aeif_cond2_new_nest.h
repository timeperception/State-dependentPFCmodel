/*
 *  aeif_mod2_new_nest.h
 *
 *  This file is part of NEST.
 *
 *  Copyright (C) 2004 The NEST Initiative
 *
 *  NEST is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  NEST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NEST.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef AEIF_MOD2_NEW_NEST_H
#define AEIF_MOD2_NEW_NEST_H

#include "config.h"

#ifdef HAVE_GSL

#include "nest_types.h"
#include "event.h"
#include "archiving_node.h"
#include "ring_buffer.h"
#include "connection.h"
#include "universal_data_logger.h"
#include "recordables_map.h"

#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv.h>

/* BeginDocumentation
Name: aeif_mod2 - simplified adaptive exponential integrate-and-fire neuron model according to Hass, Hertaeg and Durstewitz (2016),
modified from the NEST default version Conductance based exponential integrate-and-fire neuron model according to Brette and Gerstner (2005),

Description:
aeif_mod2 is the simplified adaptive exponential integrate and fire neuron (simpAdEx) according to Hertäg et al. (2012).
Incoming spike events induce a post-synaptic change of conductance modelled by a 
double exponential function with separate on and off time constants. Includes an
NMDA current with voltage-dependent Mg nonlinearity as per Durstewitz & Gabriel
2007. Some parts of code taken from the ht_neuron model.

This implementation uses the embedded 4th order Runge-Kutta-Fehlberg solver with adaptive stepsize to integrate
the differential equation.

The membrane potential is given by the following differential equation:
C dV/dt = -g_L(V-E_L)+g_L*Delta_T*exp((V-V_th)/Delta_T) + s*I - w = w_v - w

and

dw/dt = (HeavisideFunction)*(V_th-V)*[1-tau_m/tau_w] * dw_v/dV * dV/dt

Parameters: 
The following parameters can be set in the status dictionary.

Dynamic state variables:
  V_m        double - Membrane potential in mV
  g_ex       double - Excitatory synaptic conductance in nS.
  dg_ex      double - First derivative of g_ex in nS/ms
  g_in       double - Inhibitory synaptic conductance in nS.
  dg_in      double - First derivative of g_in in nS/ms.
  w          double - Spike-adaptation current in pA.

Membrane Parameters:
  C_m        double - Capacity of the membrane in pF
  t_ref      double - Duration of refractory period in ms. 
  V_reset    double - Reset value for V_m after a spike. In mV.
  E_L        double - Leak reversal potential in mV. 
  g_L        double - Leak conductance in nS.
  I_e        double - input current in pA.

Spike adaptation parameters:
  b          double - Spike-triggered adaptation in pA.
  Delta_T    double - Slope factor in mV
  tau_w      double - Adaptation time constant in ms
  V_th       double - Spike initiation threshold in mV
  V_peak     double - Spike detection threshold in mV.
  s          double - Constant of the model
  I_ref      double - Maximal input current possible

Synaptic parameters
  gpeak_AMPA      double - Peak AMPA conductance in nS
  gpeak_I         double - Peak inhibitory conductance in nS
  gpeak_NMDA      double - Peak NMDA conductnce in nS
  tau_synAMPA_on  double - On Time Constant AMPA Synapse in ms
  tau_synAMPA_off double - Off Time Constant AMPA Synapse in ms
  tau_synI_on     double - On Time Constant inhibitory Synapse in ms
  tau_synI_off    double - Off Time Constant inhibitory Synapse in ms
  tau_synNMDA_on  double - Synaptic On Time Constant Excitatory NMDA Synapse in ms
  tau_synNMDA_off double - Synaptic Off Time Constant Excitatory NMDA Synapse in ms
  Mg_gate         double
  Mg_fac          double
  Mg_slope        double
  Mg_half         double

Integration parameters
  gsl_error_tol  double - This parameter controls the admissible error of the GSL integrator.
                          Reduce it if NEST complains about numerical instabilities.

Debugers joker
  DJ      double - it can be used so a value will be recorded by a multmeter

Author: Marc-Oliver Gewaltig

modifications by Grant Sutcliffe

modifications again by Santiago Moreno

modifications again by Pirathitha Ravichandran-Schmidt


Sends: SpikeEvent

Receives: SpikeEvent, CurrentEvent, DataLoggingRequest

References: Brette R and Gerstner W (2005) Adaptive Exponential Integrate-and-Fire Model as 
            an Effective Description of Neuronal Activity. J Neurophysiol 94:3637-3642

SeeAlso: iaf_cond_alpha, aeif_cond_exp
*/

namespace mynest
{
  /**
   * Function computing right-hand side of ODE for GSL solver.
   * @note Must be declared here so we can befriend it in class.
   * @note Must have C-linkage for passing to GSL. Internally, it is
   *       a first-class C++ function, but cannot be a member function
   *       because of the C-linkage.
   * @note No point in declaring it inline, since it is called
   *       through a function pointer.
   * @param void* Pointer to model neuron instance.
   */
  extern "C"
  int aeif_mod2_new_nest_dynamics (double, const double*, double*, void*);

  class aeif_mod2_new_nest:
  public nest::Archiving_Node
  {
    
  public:        
    
    aeif_mod2_new_nest();
    aeif_mod2_new_nest(const aeif_mod2_new_nest&);
    ~aeif_mod2_new_nest();

    /**
     * Import sets of overloaded virtual functions.
     * We need to explicitly include sets of overloaded
     * virtual functions into the current scope.
     * According to the SUN C++ FAQ, this is the correct
     * way of doing things, although all other compilers
     * happily live without.
     */

    using Node::handles_test_event;
    using Node::handle;

    //nest::port check_connection(nest::Connection&, nest::port);
    
	nest::port send_test_event( Node&, nest::rport, nest::synindex, bool );
	
    void handle(nest::SpikeEvent &);
    void handle(nest::CurrentEvent &);
    void handle(nest::DataLoggingRequest &); 
    
    //nest::port connect_sender(nest::SpikeEvent &, nest::port);
    //nest::port connect_sender(nest::CurrentEvent &, nest::port);
    //nest::port connect_sender(nest::DataLoggingRequest &, nest::port);

	nest::port handles_test_event( nest::SpikeEvent&, nest::rport );
	nest::port handles_test_event( nest::CurrentEvent&, nest::rport );
	nest::port handles_test_event( nest::DataLoggingRequest&, nest::rport );
	
    void get_status(DictionaryDatum &) const;
    void set_status(const DictionaryDatum &);
    
  private:
    
    void init_state_(const Node& proto);
    void init_buffers_();
    double get_synapse_constant(double, double, double) const;
	double_t sgate() const;
    void calibrate();
    void update(nest::Time const &, const long, const long);

    // END Boilerplate function declarations ----------------------------

    // Friends --------------------------------------------------------

    // make dynamics function quasi-member
    friend int aeif_mod2_new_nest_dynamics(double, const double*, double*, void*);

    // The next two classes need to be friends to access the State_ class/member
    friend class nest::RecordablesMap<aeif_mod2_new_nest>;
    friend class nest::UniversalDataLogger<aeif_mod2_new_nest>;

  private:

    // ---------------------------------------------------------------- 

    //! Independent parameters
    struct Parameters_ {
      double V_peak_;     //!< Spike detection threshold in mV
      double V_reset_;    //!< Reset Potential in mV
      double t_ref_;      //!< Refractory period in ms
	  
      double g_L;         //!< Leak Conductance in nS
      double C_m;         //!< Membrane Capacitance in pF
      double E_ex;        //!< Excitatory reversal Potential in mV
      double E_in;        //!< Inhibitory reversal Potential in mV
      double E_inh_b;     //!< Inhibitory reversal Potential GABAB in mV
      double E_L;         //!< Leak reversal Potential (aka resting potential) in mV
      double Delta_T;     //!< Slope faktor in ms.
      double tau_w;       //!< adaptation time-constant in ms.
      double a;           //!< Subthreshold adaptation in nS.
      double b;           //!< Spike-triggered adaptation in pA
      double V_th;        //!< Spike threshold in mV.
      double I_e;         //!< Intrinsic current in pA.
      double s;           //!< scaling factor.
      //nest::double_t DJ;           //!< Just to check something (debuger's joker)
	  double I_ref;      //!< I_ref period in pA
	  double V_dep;      //!< I_ref period in pA

	  double gpeak_AMPA;         //!< Peak AMPA conductance in nS
	  double gpeak_I;        //!< Peak inhibitory conductance in nS
	  double gpeak_Inhb;
	  double gpeak_NMDA;     //!< Peak NMDA conductance in nS
      double tau_synAMPA_on;  //!< On Time Constant AMPA Synapse in ms
      double tau_synAMPA_off; //!< Off Time Constant AMPA Synapse in ms
      double tau_synI_on;     //!< On Time Constant inhibitory Synapse in ms
      double tau_synI_off;    //!< Off Time Constant inhibitory Synapse in ms
      double tau_synNMDA_on;  //!< Synaptic On Time Constant Excitatory NMDA Synapse in ms
	  double tau_synNMDA_off; //!< Synaptic Off Time Constant Excitatory NMDA Synapse in ms
	  double tau_synInhb_on;  //!< Synaptic On Time Constant Excitatory NMDA Synapse in ms
	  double tau_synInhb_off; //!< Synaptic Off Time Constant Excitatory NMDA Synapse in ms

	  double Mg_gate;
	  double Mg_fac;
	  double Mg_slope;
	  double Mg_half;
	
	  double gsl_error_tol;   //!< error bound for GSL integrator
  
      Parameters_();  //!< Sets default parameter values

      void get(DictionaryDatum&) const;  //!< Store current values in dictionary
      void set(const DictionaryDatum&);  //!< Set values from dicitonary
    };

  public:
    // ---------------------------------------------------------------- 

    /**
     * State variables of the model.
     * @note Copy constructor and assignment operator required because
     *       of C-style array.
     */
    struct State_
    {
      /**
       * Enumeration identifying elements in state array State_::y_.
       * The state vector must be passed to GSL as a C array. This enum
       * identifies the elements of the vector. It must be public to be
       * accessible from the iteration function.
       */  
      enum StateVecElems
      {
	V_M   = 0,
	DG_EXC   ,  // 1
	G_EXC    ,  // 2
	DG_INH   ,  // 3
	G_INH    ,  // 4
	W        ,  // 5
	DG_EXC_N ,
	G_EXC_N  ,
	DG_INH_B  ,
	G_INH_B ,
	STATE_VEC_SIZE
      };

      double y_[STATE_VEC_SIZE];  //!< neuron state, must be C-array for GSL solver
      int    r_;                  //!< number of refractory steps remaining

      State_(const Parameters_&);  //!< Default initialization
      State_(const State_&);
      State_& operator=(const State_&);

      void get(DictionaryDatum&) const;
      void set(const DictionaryDatum&, const Parameters_&);
    };    

    // ---------------------------------------------------------------- 

    /**
     * Buffers of the model.
     */
    struct Buffers_ {
      Buffers_(aeif_mod2_new_nest&);                   //!<Sets buffer pointers to 0
      Buffers_(const Buffers_&, aeif_mod2_new_nest&);  //!<Sets buffer pointers to 0

      //! Logger for all analog data
      nest::UniversalDataLogger<aeif_mod2_new_nest> logger_;

      /** buffers and sums up incoming spikes/currents */
      nest::RingBuffer spike_exc_;
	  nest::RingBuffer spike_exc_n_;
      nest::RingBuffer spike_inh_;
      nest::RingBuffer spike_inh_b_;
      nest::RingBuffer currents_;

      /** GSL ODE stuff */
      gsl_odeiv_step*    s_;    //!< stepping function
      gsl_odeiv_control* c_;    //!< adaptive stepsize control function
      gsl_odeiv_evolve*  e_;    //!< evolution function
      gsl_odeiv_system   sys_;  //!< struct describing system
      
      // IntergrationStep_ should be reset with the neuron on ResetNetwork,
      // but remain unchanged during calibration. Since it is initialized with
      // step_, and the resolution cannot change after nodes have been created,
      // it is safe to place both here.
      double step_;           //!< step size in ms
      double   IntegrationStep_;//!< current integration time step, updated by GSL

      /** 
       * Input current injected by CurrentEvent.
       * This variable is used to transport the current applied into the
       * _dynamics function computing the derivative of the state vector.
       * It must be a part of Buffers_, since it is initialized once before
       * the first simulation, but not modified before later Simulate calls.
       */
      double I_stim_;
    };

     // ---------------------------------------------------------------- 

     /**
      * Internal variables of the model.
      */
     struct Variables_ { 
      /** initial value to normalise excitatory synaptic conductance */
      double g0_ex_; 

      /** initial value to normalise NMDA synaptic conductance */
      double g0_ex_n; 
   
      /** initial value to normalise inhibitory synaptic conductance */
      double g0_in_;    

      double g0_inh_b;


      int   RefractoryCounts_;
     };

    // Access functions for UniversalDataLogger -------------------------------
    
    //! Read out state vector elements, used by UniversalDataLogger
    template <State_::StateVecElems elem>
    double get_y_elem_() const { return S_.y_[elem]; }
	double get_g_ex_n_() const { return S_.y_[State_::G_EXC_N]*sgate(); }
    double get_g_inh_b_() const { return S_.y_[State_::G_INH_B]; }

    // ---------------------------------------------------------------- 

    Parameters_ P_;
    State_      S_;
    Variables_  V_;
    Buffers_    B_;

    //! Mapping of recordables names to access functions
    static nest::RecordablesMap<aeif_mod2_new_nest> recordablesMap_;
  };

  inline 
  nest::port 
  mynest::aeif_mod2_new_nest::send_test_event( Node& target, nest::rport receptor_type, nest::synindex, bool )
  {
    nest::SpikeEvent e;
    e.set_sender( *this );
    
    return target.handles_test_event( e, receptor_type );
  }


 
 
  inline
  nest::port aeif_mod2_new_nest::handles_test_event(nest::CurrentEvent&, nest::port receptor_type)
  {
    if (receptor_type != 0)
      throw nest::UnknownReceptorType(receptor_type, get_name());
    return 0;
  }

  inline
  nest::port aeif_mod2_new_nest::handles_test_event(nest::DataLoggingRequest& dlr, 
				      nest::port receptor_type)
  {
    if (receptor_type != 0)
      throw nest::UnknownReceptorType(receptor_type, get_name());
    return B_.logger_.connect_logging_device(dlr, recordablesMap_);
  }
 
  inline
  void aeif_mod2_new_nest::get_status(DictionaryDatum &d) const
  {
    P_.get(d);
    S_.get(d);
    Archiving_Node::get_status(d);

    (*d)[nest::names::recordables] = recordablesMap_.get_list();
  }

  inline
  void aeif_mod2_new_nest::set_status(const DictionaryDatum &d)
  {
    Parameters_ ptmp = P_;  // temporary copy in case of errors
    ptmp.set(d);                       // throws if BadProperty
    State_      stmp = S_;  // temporary copy in case of errors
    stmp.set(d, ptmp);                 // throws if BadProperty

    // We now know that (ptmp, stmp) are consistent. We do not 
    // write them back to (P_, S_) before we are also sure that 
    // the properties to be set in the parent class are internally 
    // consistent.
    Archiving_Node::set_status(d);

    // if we get here, temporaries contain consistent set of properties
    P_ = ptmp;
    S_ = stmp;
  }
  
} // namespace

#endif //HAVE_GSL_1_11
#endif //AEIF_MOD2_NEW_NEST_H
