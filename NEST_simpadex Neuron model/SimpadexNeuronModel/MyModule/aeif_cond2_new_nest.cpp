/*
 *  aeif_mod2_new_nest_new_nest.cpp
 *
 *  This file is part of NEST.
 *
 *  Copyright (C) 2004 The NEST Initiative
 *
 *  NEST is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  NEST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NEST.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "aeif_cond2_new_nest.h"
#ifdef HAVE_GSL

#include <limits>
//#include <cassert>
#include <cmath>
#include <iomanip>
#include <iostream>
//#include <fstream>
#include <cstdio>

#include "numerics.h"

#include "exceptions.h"
#include "nest_names.h"
#include "kernel_manager.h"
#include "universal_data_logger_impl.h"

//#include "network.h"
#include "dict.h"
#include "integerdatum.h"
#include "doubledatum.h"
#include "dictutils.h"
//#include <gsl/gsl_odeiv2.h>

using namespace nest;

/* ----------------------------------------------------------------
 * Recordables map
 * ---------------------------------------------------------------- */

nest::RecordablesMap<mynest::aeif_mod2_new_nest> mynest::aeif_mod2_new_nest::recordablesMap_;

namespace nest  // template specialization must be placed in namespace
{
  // Override the create() method with one call to RecordablesMap::insert_() 
  // for each quantity to be recorded.
  template <>
  void RecordablesMap<mynest::aeif_mod2_new_nest>::create()
  {
    // use standard names whereever you can for consistency!
    insert_(names::V_m, 
	    &mynest::aeif_mod2_new_nest::get_y_elem_<mynest::aeif_mod2_new_nest::State_::V_M>);
    insert_(names::g_ex, 
	    &mynest::aeif_mod2_new_nest::get_y_elem_<mynest::aeif_mod2_new_nest::State_::G_EXC>);
    insert_(names::g_in, 
	    &mynest::aeif_mod2_new_nest::get_y_elem_<mynest::aeif_mod2_new_nest::State_::G_INH>);
    insert_(names::w, 
	    &mynest::aeif_mod2_new_nest::get_y_elem_<mynest::aeif_mod2_new_nest::State_::W>);
    insert_(Name("g_ex_n"),
	    &mynest::aeif_mod2_new_nest::get_g_ex_n_);
    insert_(Name("g_inh_b"),
	    &mynest::aeif_mod2_new_nest::get_g_inh_b_);

  }
}

extern "C"
int mynest::aeif_mod2_new_nest_dynamics (double, const double y[], double f[], void* pnode)
{
  // a shorthand
  typedef mynest::aeif_mod2_new_nest::State_ S;

  // get access to node so we can almost work as in a member function
  assert(pnode);
  const mynest::aeif_mod2_new_nest& node =  *(reinterpret_cast<mynest::aeif_mod2_new_nest*>(pnode));

  // y[] here is---and must be---the state vector supplied by the integrator,
  // not the state vector in the node, node.S_.y[]. 
  
  // The following code is verbose for the sake of clarity. We assume that a
  // good compiler will optimize the verbosity away ...

  // This constant is used below as the largest admissible value for the exponential spike upstroke
  static const double largest_exp=std::exp(10.);

 

  // shorthand for state variables
  const double& V         = y[S::V_M];
  const double& dg_ex     = y[S::DG_EXC];
  const double&  g_ex     = y[S::G_EXC ];
  const double& dg_in     = y[S::DG_INH];
  const double&  g_in     = y[S::G_INH ];
  const double& w         = y[S::W];
  const double& dg_ex_n   = y[S::DG_EXC_N];
  const double& g_ex_n   = y[S::G_EXC_N];
  const double& dg_inh_b   = y[S::DG_INH_B];
  const double& g_inh_b   = y[S::G_INH_B];
 // NMDA current
 

  const double sgate = node.P_.Mg_gate / float( 1.0 + node.P_.Mg_fac * std::exp( node.P_.Mg_slope * ( -V )));
  
  const double I_syn_exc = g_ex * ( node.P_.E_ex-V);
  const double I_syn_inh = g_in * (node.P_.E_in-V);
  const double I_syn_exc_n = sgate * g_ex_n * ( node.P_.E_ex -V );
  const double I_syn_inh_b = g_inh_b * (node.P_.E_inh_b-V);

  // This code permits to show some informations from a precise neuron
  // Used to verify the NMDA voltage dependance
  // double b = 1.06;
  // if ( node.P_.b >= b-0.001 && node.P_.b < b+0.001){
  //   printf("sgate: %f, V: %f, I_syn_exc_n: %f, I_syn_exc_aux: %f\n", sgate, V, I_syn_exc_n, I_syn_exc_aux);
  //}

  // We pre-compute the argument of the exponential
  const double exp_arg=(V - node.P_.V_th) / node.P_.Delta_T;
  // If the argument is too large, we clip it.
  const double I_spike =  node.P_.g_L* node.P_.Delta_T * std::exp(exp_arg);
  //const double I_tot = node.P_.I_e+I_syn_exc + I_syn_inh +I_syn_exc_n;
  const double I_syn = I_syn_exc + I_syn_inh + I_syn_exc_n+ I_syn_inh_b;
  const double wV = node.P_.I_e + I_syn - node.P_.g_L* (V-node.P_.E_L) +I_spike ;
  const double D0 = node.P_.C_m / node.P_.g_L*wV;
  //const nest::double tau_m = node.P_.C_m / node.P_.g_L;
  

  // dv/dt
  //if((node.P_.I_e + I_syn) >= node.P_.I_ref){
  //	f[S::V_M  ] = -(node.P_.g_L / node.P_.C_m) *(V - node.P_.V_dep);
  //}
  //else{
  f[S::V_M  ] = (node.P_.I_e - node.P_.g_L*(V-node.P_.E_L) - w +I_syn + I_spike + node.B_.I_stim_)/node.P_.C_m;
  //}  
  const double dD0 = node.P_.C_m*(std::exp(exp_arg)-1);
  
  
  bool test = true;
  if(w <= wV - D0/node.P_.tau_w){
	//std::cout<<"w<wV "<<w<<std::endl;
    test = false;
  }
  if(w >= wV + D0/node.P_.tau_w){
	//std::cout<<"w>wV "<<w<<std::endl;
    test = false;
  }

  if(V>node.P_.V_th){
	//std::cout<<"V>V_th "<<V<<std::endl;
    test=false;
  }

  if((node.P_.I_e + I_syn) >= node.P_.I_ref){
	  //std::cout<<"I_e_Isyn "<<(node.P_.I_e + I_syn)<<std::endl;
	  test=false;
	  
  }

  if(test){
    f[S::W    ] = -(node.P_.g_L*(1-std::exp(exp_arg)) + dD0/node.P_.tau_w)*f[S::V_M  ];
  }
  else{
	 f[S::W    ] = 0;
  }

  f[S::DG_EXC] = -y[ S::DG_EXC ] / node.P_.tau_synAMPA_on;
  f[S::G_EXC ] =  y[ S::DG_EXC ] - y[ S::G_EXC ] / node.P_.tau_synAMPA_off; // Synaptic Conductance (nS)

  f[S::DG_INH] = -y[ S::DG_INH ] / node.P_.tau_synI_on;
  f[S::G_INH ] =  y[ S::DG_INH ] - y[ S::G_INH ] / node.P_.tau_synI_off; // Synaptic Conductance (nS)

  f[S::DG_EXC_N] =  -y[ S::DG_EXC_N ] / node.P_.tau_synNMDA_on;
  f[S::G_EXC_N ] =  y[ S::DG_EXC_N ] - y[ S::G_EXC_N ] / node.P_.tau_synNMDA_off; // Synaptic Conductance (nS)

  f[S::DG_INH_B] =  -y[ S::DG_INH_B ] / node.P_.tau_synInhb_on;
  f[S::G_INH_B ] =  y[ S::DG_INH_B ] - y[ S::G_INH_B ] / node.P_.tau_synInhb_off; // Synaptic Conductance (nS)


  // Adaptation current w.

  //This is divided in three parts to facilitate
  // the debugging work

  
 

  return GSL_SUCCESS;
}

/* ----------------------------------------------------------------
 * Default constructors defining default parameters and state
 * ---------------------------------------------------------------- */

mynest::aeif_mod2_new_nest::Parameters_::Parameters_()
  : V_peak_    		(   0.0   ),  // mV, should not be larger that V_th+10
    V_reset_   		(-60.00   ),  // mV
    t_ref_     		(   0.0   ),  // ms
    g_L        		(  5.00   ),  // nS
    C_m        		(200.00   ),  // pF
    E_ex       		(   0.0   ),  // mV
    E_in       		( -85.0   ),  // mV
    E_inh_b         ( -90.0   ), // Quelle: https://books.google.de/books?id=M-1JDwAAQBAJ&pg=PA171&lpg=PA171&dq=gabab+time+constants+modelling&source=bl&ots=6azU0xnThF&sig=ACfU3U3jLEu017D5WCmc3a5SeNfS0YrpjQ&hl=de&sa=X&ved=2ahUKEwiCo8jP3YDpAhXgwcQBHVe5D-wQ6AEwA3oECAkQAQ#v=onepage&q=gabab%20time%20constants%20modelling&f=false
    E_L        		(-70.00   ),  // mV
    Delta_T    		(  2.00   ),  // mV
    tau_w      		(200.00   ),  // ms
    a          		(   0.0   ),  // nS
    b          		( 5.000   ),  // pA
    V_th       		(-50.00   ),  // mV
    I_e        		(   0.0   ),  // pA
    s               (   1.0   ),  // no units
    //DJ              (   0.0   ),  // just to check something
	I_ref			(   1.0   ), //I_ref
	V_dep			(   0.0   ), //I_ref
    gpeak_AMPA		(  1.0	  ),
    gpeak_I			(  1.0	  ),
    gpeak_Inhb      (  1.0    ),
    gpeak_NMDA		(  1.09	  ),
    tau_synAMPA_on	(  0.2    ),  // ms
    tau_synAMPA_off	(  2.0    ),  // ms
    tau_synI_on		(  2.0    ),  // ms
    tau_synI_off	(  6.0    ),  // ms
    tau_synNMDA_on	(  2.0    ),  // ms
    tau_synNMDA_off	(  20.0   ),  // ms
    tau_synInhb_on (  2.0    ), // ms
    tau_synInhb_off  (  150.0  ), // Quelle siehe oben
	Mg_gate			( 1.50265 ),
	Mg_fac			(  0.33	  ),
	Mg_slope		(  0.0625 ),
	Mg_half			(  0.0	  ),
    gsl_error_tol( 1e-6  )
{
}

mynest::aeif_mod2_new_nest::State_::State_(const Parameters_& p)
  : r_(0)
{
  y_[0] = p.E_L;
  for ( size_t i = 1 ; i < STATE_VEC_SIZE ; ++i )
    y_[i] = 0;
}

mynest::aeif_mod2_new_nest::State_::State_(const State_& s)
  : r_(s.r_)
{
  for ( size_t i = 0 ; i < STATE_VEC_SIZE ; ++i )
    y_[i] = s.y_[i];
}

mynest::aeif_mod2_new_nest::State_& mynest::aeif_mod2_new_nest::State_::operator=(const State_& s)
{
  assert(this != &s);  // would be bad logical error in program

  for ( size_t i = 0 ; i < STATE_VEC_SIZE ; ++i )
    y_[i] = s.y_[i];
  r_ = s.r_;
  return *this;
}

/* ----------------------------------------------------------------
 * Parameter and state extractions and manipulation functions
 * ---------------------------------------------------------------- */

void mynest::aeif_mod2_new_nest::Parameters_::get(DictionaryDatum &d) const
{
  def<double>(d,names::C_m,    C_m);
  def<double>(d,names::V_th,   V_th);
  def<double>(d,names::t_ref,  t_ref_);
  def<double>(d,names::g_L,    g_L);
  def<double>(d,names::E_L,    E_L);
  def<double>(d,names::V_reset,V_reset_);
  def<double>(d,names::E_ex,   E_ex);
  def<double>(d,names::E_in,   E_in);
  //def<double>(d,names::E_inh_b,   E_inh_b);

  def<double>(d,names::a,      a);
  def<double>(d,names::b,      b);
  def<double>(d,names::Delta_T,Delta_T);
  def<double>(d,names::tau_w,  tau_w);
  def<double>(d,names::I_e,    I_e);
  //def<double>(d,Name("DJ"),    DJ);
  def<double>(d,names::V_peak, V_peak_);
  def<double>(d,Name("gpeak_AMPA"),   		gpeak_AMPA);
  def<double>(d,Name("gpeak_Inhb"),   		gpeak_Inhb);
  def<double>(d,Name("E_inh_b"),   		E_inh_b);
  def<double>(d,Name("gpeak_I"),   			gpeak_I);
  def<double>(d,Name("gpeak_NMDA"),   		gpeak_NMDA);
  def<double>(d,Name("tau_syn_AMPA_on"),      tau_synAMPA_on);
  def<double>(d,Name("tau_syn_AMPA_off"),     tau_synAMPA_off);
  def<double>(d,Name("tau_syn_in_on"),   	    tau_synI_on);
  def<double>(d,Name("tau_syn_in_off"),   	tau_synI_off);
  def<double>(d,Name("tau_syn_NMDA_on"),      tau_synNMDA_on);
  def<double>(d,Name("tau_syn_NMDA_off"),     tau_synNMDA_off);
  def<double>(d,Name("tau_syn_Inhb_on"),      tau_synInhb_on);
  def<double>(d,Name("tau_syn_Inhb_off"),     tau_synInhb_off);

  def<double>(d,Name("Mg_gate"),			    Mg_gate);
  def<double>(d,Name("Mg_fac"),				Mg_fac);
  def<double>(d,Name("Mg_slope"),			    Mg_slope);
  def<double>(d,Name("Mg_half"),			    Mg_half);
  def<double>(d,Name("I_ref"), 				   I_ref);
  def<double>(d,Name("V_dep"), 				   V_dep);
  def<double>(d,names::gsl_error_tol, gsl_error_tol);
}

void mynest::aeif_mod2_new_nest::Parameters_::set(const DictionaryDatum& d)
{
  updateValue<double>(d,names::V_th,    V_th);
  updateValue<double>(d,names::V_peak,  V_peak_);
  updateValue<double>(d,names::t_ref,   t_ref_);
  updateValue<double>(d,names::E_L,     E_L);
  updateValue<double>(d,names::V_reset, V_reset_);
  updateValue<double>(d,names::E_ex,    E_ex);
  updateValue<double>(d,names::E_in,    E_in);
  //updateValue<double>(d,names::E_inh_b,    E_inh_b);
  updateValue<double>(d,Name("E_inh_b"),   	E_inh_b);

  updateValue<double>(d,names::C_m,     C_m);
  updateValue<double>(d,names::g_L,     g_L);

  updateValue<double>(d,names::a,      a);
  updateValue<double>(d,names::b,      b);
  updateValue<double>(d,names::Delta_T,Delta_T);
  updateValue<double>(d,names::tau_w,  tau_w);

  updateValue<double>(d,names::I_e,    I_e);
  updateValue<double>(d,Name("I_ref"), 				   I_ref);
  updateValue<double>(d,Name("V_dep"), 				   V_dep);

  //updateValue<double>(d,Name("DJ"),    DJ);

  updateValue<double>(d,Name("gpeak_AMPA"),   	gpeak_AMPA);
  updateValue<double>(d,Name("gpeak_I"),   		gpeak_I);
  updateValue<double>(d,Name("gpeak_Inhb"),   		gpeak_Inhb);
  updateValue<double>(d,Name("gpeak_NMDA"),   	gpeak_NMDA);

  updateValue<double>(d,Name("tau_syn_AMPA_on"), 	tau_synAMPA_on);
  updateValue<double>(d,Name("tau_syn_AMPA_off"), tau_synAMPA_off);
  updateValue<double>(d,Name("tau_syn_in_on"), 	tau_synI_on);
  updateValue<double>(d,Name("tau_syn_in_off"), 	tau_synI_off);
  updateValue<double>(d,Name("tau_syn_NMDA_on"), 	tau_synNMDA_on);
  updateValue<double>(d,Name("tau_syn_NMDA_off"), tau_synNMDA_off);
  updateValue<double>(d,Name("tau_syn_Inhb_on"), 	tau_synInhb_on);
  updateValue<double>(d,Name("tau_syn_Inhb_off"), tau_synInhb_off);

  updateValue<double>(d,Name("Mg_gate"), 			Mg_gate);
  updateValue<double>(d,Name("Mg_fac"), 			Mg_fac);
  updateValue<double>(d,Name("Mg_slope"), 		Mg_slope);
  updateValue<double>(d,Name("Mg_half"), 			Mg_half);

  updateValue<double>(d,names::gsl_error_tol, gsl_error_tol);

  if ( V_peak_ <= V_th )
    throw BadProperty("V_peak must be larger than threshold.");

  if ( V_reset_ >= V_peak_ )
    throw BadProperty("Ensure that: V_reset < V_peak .");

  if ( C_m <= 0 )
  {
    throw BadProperty("Capacitance must be strictly positive.");
  }

  if ( t_ref_ < 0 )
    throw BadProperty("Refractory time cannot be negative.");

  if ( tau_synAMPA_on <= 0 || tau_synAMPA_off <= 0 || tau_synI_on <= 0 || tau_synI_off <= 0 || tau_synNMDA_on <= 0 || tau_synNMDA_off <= 0 || tau_synInhb_on <= 0 || tau_synInhb_off <= 0 || tau_w <= 0 )
    throw BadProperty("All time constants must be strictly positive.");

  if ( gsl_error_tol <= 0. )
    throw BadProperty("The gsl_error_tol must be strictly positive.");
}

void mynest::aeif_mod2_new_nest::State_::get(DictionaryDatum &d) const
{
  def<double>(d,names::V_m,    y_[V_M]);
  def<double>(d,names::g_ex,   y_[G_EXC]);
  def<double>(d,names::dg_ex,  y_[DG_EXC]);
  def<double>(d,names::g_in,   y_[G_INH]);
  def<double>(d,names::dg_in,  y_[DG_INH]);
  def<double>(d,names::w,      y_[W]);
  def<double>(d,Name("g_ex_n"), y_[G_EXC_N]);
  def<double>(d,Name("dg_ex_n"), y_[DG_EXC_N]);
  def<double>(d,Name("g_inh_b"), y_[G_INH_B]);
  def<double>(d,Name("dg_inh_b"), y_[DG_INH_B]);

  //def<double>(d,Name("DJ"), y_[DJ]);
}

void mynest::aeif_mod2_new_nest::State_::set(const DictionaryDatum& d, const Parameters_&)
{
  updateValue<double>(d,names::V_m,   y_[V_M]);
  updateValue<double>(d,names::g_ex,  y_[G_EXC]);
  updateValue<double>(d,names::dg_ex, y_[DG_EXC]);
  updateValue<double>(d,names::g_in,  y_[G_INH]);
  updateValue<double>(d,names::dg_in, y_[DG_INH]);
  updateValue<double>(d,names::w,     y_[W]);
  updateValue<double>(d,Name("g_ex_n"),  y_[G_EXC_N]);
  updateValue<double>(d,Name("dg_ex_n"), y_[DG_EXC_N]);
  updateValue<double>(d,Name("g_inh_b"), y_[G_INH_B]);
  updateValue<double>(d,Name("dg_inh_b"), y_[DG_INH_B]);
  //updateValue<double>(d,Name("DJ"), y_[DJ]);

  if ( y_[G_EXC] < 0 || y_[G_INH] < 0 || y_[G_EXC_N] < 0 || y_[G_INH_B] < 0 )
    throw BadProperty("Conductances must not be negative.");
}

mynest::aeif_mod2_new_nest::Buffers_::Buffers_(aeif_mod2_new_nest& n)
  : logger_(n),
    s_(0),
    c_(0),
    e_(0)
{
  // Initialization of the remaining members is deferred to
  // init_buffers_().
}

mynest::aeif_mod2_new_nest::Buffers_::Buffers_(const Buffers_&, aeif_mod2_new_nest& n)
  : logger_(n),
    s_(0),
    c_(0),
    e_(0)
{
  // Initialization of the remaining members is deferred to
  // init_buffers_().
}

/* ----------------------------------------------------------------
 * Default and copy constructor for node, and destructor
 * ---------------------------------------------------------------- */

mynest::aeif_mod2_new_nest::aeif_mod2_new_nest()
  : Archiving_Node(),
    P_(),
    S_(P_),
    B_(*this)
{
  recordablesMap_.create();
}

mynest::aeif_mod2_new_nest::aeif_mod2_new_nest(const aeif_mod2_new_nest& n)
  : Archiving_Node(n),
    P_(n.P_),
    S_(n.S_),
    B_(n.B_, *this)
{
}
								
mynest::aeif_mod2_new_nest::~aeif_mod2_new_nest()
{
  // GSL structs may not have been allocated, so we need to protect destruction
  if ( B_.s_ ) gsl_odeiv_step_free(B_.s_);
  if ( B_.c_ ) gsl_odeiv_control_free(B_.c_);
  if ( B_.e_ ) gsl_odeiv_evolve_free(B_.e_);
}

/* ----------------------------------------------------------------
 * Node initialization functions
 * ---------------------------------------------------------------- */

void mynest::aeif_mod2_new_nest::init_state_(const Node& proto)
{
  const aeif_mod2_new_nest& pr = downcast<aeif_mod2_new_nest>(proto);
  S_ = pr.S_;
}

void mynest::aeif_mod2_new_nest::init_buffers_()
{
  B_.spike_exc_.clear();          // includes resize
  B_.spike_exc_n_.clear();          // includes resize
  B_.spike_inh_.clear();          // includes resize
  B_.spike_inh_b_.clear();          //
  B_.currents_.clear();           // includes resize
  Archiving_Node::clear_history();

  B_.logger_.reset();

  B_.step_ = Time::get_resolution().get_ms();

  // We must integrate this model with high-precision to obtain decent results
  B_.IntegrationStep_ = std::min(0.01, B_.step_);

  static const gsl_odeiv_step_type* T1 = gsl_odeiv_step_rk2;

  if ( B_.s_ == 0 )
    B_.s_ = gsl_odeiv_step_alloc (T1, State_::STATE_VEC_SIZE);
  else
    gsl_odeiv_step_reset(B_.s_);

  if ( B_.c_ == 0 )
    B_.c_ = gsl_odeiv_control_yp_new (P_.gsl_error_tol, P_.gsl_error_tol);
  else
    gsl_odeiv_control_init(B_.c_, P_.gsl_error_tol, P_.gsl_error_tol, 0.0, 1.0);

  if ( B_.e_ == 0 )
    B_.e_ = gsl_odeiv_evolve_alloc(State_::STATE_VEC_SIZE);
  else
    gsl_odeiv_evolve_reset(B_.e_);

  B_.sys_.function  = aeif_mod2_new_nest_dynamics;
  B_.sys_.jacobian  = NULL;
  B_.sys_.dimension = State_::STATE_VEC_SIZE;
  B_.sys_.params    = reinterpret_cast<void*>(this);

  B_.I_stim_ = 0.0;
}
double mynest::aeif_mod2_new_nest::sgate() const
{
  return P_.Mg_gate / float( 1.0 + P_.Mg_fac * std::exp( P_.Mg_slope * (  - S_.y_[State_::V_M] )));
}
// Spike calibration function lifted from ht_neuron.cpp
double mynest::aeif_mod2_new_nest::get_synapse_constant(double tau_rise,
						      double tau_decay,
						      double ) const
{
  // Factor used to account for the missing 1/((1/Tau_2)-(1/Tau_1)) term
  // in the ht_neuron_dynamics integration of the synapse terms. 
  // See: Exact digital simulation of time-invariant linear systems
  // with applications to neuronal modeling, Rotter and Diesmann, 
  // section 3.1.2.
  //double exact_integration_adjustment = (1/Tau_2)-(1/Tau_1);
  //double t_peak = (Tau_2*Tau_1)*std::log(Tau_2/Tau_1)/(Tau_2-Tau_1);
  //double normalisation_factor = 1/(std::exp(-t_peak/Tau_1) - std::exp(-t_peak/Tau_2));
    double denom1 = tau_decay  - tau_rise;
    double denom2 = 0;
    if ( denom1 != 0 )
    {
      // peak time
      const double 
	  t_p = tau_decay * tau_rise
        * std::log( tau_decay / tau_rise ) / denom1;
      // another denominator is computed here to check that it is != 0
      denom2 = std::exp( -t_p / tau_decay )
        - std::exp( -t_p / tau_rise );
    }
    if ( denom2 == 0 ) // if rise time == decay time use alpha function
    {                  // use normalization for alpha function in this case
      return 1.0 * numerics::e / tau_decay;
    }
    else // if rise time != decay time use beta function
    {
      return ( 1. / tau_rise - 1. / tau_decay ) / denom2;
    }


  //return g_peak;//*exact_integration_adjustment*normalisation_factor;
}

void mynest::aeif_mod2_new_nest::calibrate()
{
  B_.logger_.init();  // ensures initialization in case mm connected after Simulate

  V_.g0_ex_  = P_.gpeak_AMPA*get_synapse_constant(P_.tau_synAMPA_on, P_.tau_synAMPA_off, P_.gpeak_AMPA);
  //get_synapse_constant(P_.tau_synAMPA_on, P_.tau_synAMPA_off, P_.gpeak_AMPA);
  //std::cout<<get_synapse_constant(P_.tau_synAMPA_on, P_.tau_synAMPA_off, P_.gpeak_AMPA)<<std::endl;
  V_.g0_in_  =P_.gpeak_I*get_synapse_constant(P_.tau_synI_on, P_.tau_synI_off, P_.gpeak_I);
  //std::cout<<get_synapse_constant(P_.tau_synI_on, P_.tau_synI_off, P_.gpeak_I)<<std::endl;
  
  V_.g0_ex_n  =P_.gpeak_NMDA* get_synapse_constant(P_.tau_synNMDA_on, P_.tau_synNMDA_off, P_.gpeak_NMDA);
  V_.g0_inh_b  =P_.gpeak_Inhb* get_synapse_constant(P_.tau_synInhb_on, P_.tau_synInhb_off, P_.gpeak_Inhb);

  //std::cout<<get_synapse_constant(P_.tau_synNMDA_on, P_.tau_synNMDA_off, P_.gpeak_NMDA)<<std::endl;
  V_.RefractoryCounts_ = Time(Time::ms(P_.t_ref_)).get_steps();
  assert(V_.RefractoryCounts_ >= 0);  // since t_ref_ >= 0, this can only fail in error
}


/* ----------------------------------------------------------------
 * Update and spike handling functions
 * ---------------------------------------------------------------- */

void mynest::aeif_mod2_new_nest::update(Time const & origin, const long from, const long to)
{
  

  assert( from < to );

  assert( State_::V_M == 0 );



  for ( long lag = from; lag < to; ++lag )
  assert ( to >= 0 && (delay) from < kernel().connection_manager.get_min_delay() );
  assert ( from < to );
  assert ( State_::V_M == 0 );

  for ( long lag = from; lag < to; ++lag )
    {
      double t = 0.0;
      double espike_temp = 0.0; 

      if ( S_.r_ > 0 )
	--S_.r_;
      
      // numerical integration with adaptive step size control:
      // ------------------------------------------------------
      // gsl_odeiv_evolve_apply performs only a single numerical
      // integration step, starting from t and bounded by step;
      // the while-loop ensures integration over the whole simulation
      // step (0, step] if more than one integration step is needed due
      // to a small integration step size;
      // note that (t+IntegrationStep > step) leads to integration over
      // (t, step] and afterwards setting t to step, but it does not
      // enforce setting IntegrationStep to step-t; this is of advantage
      // for a consistent and efficient integration across subsequent
      // simulation intervals
      
      while ( t < B_.step_ )
	{
	  if ( B_.IntegrationStep_ < 0.00000000001) {std::printf("stability problem\n");break;} // To prevent infinite loops from 
	                                                                                        // a stability problem with the solver
	                                                                                        // If theres is any problem with this, check
	  //pre                                                                                       // your equations and solve it.
	  const double vp = S_.y_[State_::V_M];
	  const double t0_i = t;
	  const double wp = S_.y_[State_::W];
	  //std::cout<<"t0 "<<t0_i<<std::endl;
	  
	  // integration 
	  const int status = gsl_odeiv_evolve_apply(B_.e_, B_.c_, B_.s_,
						    &B_.sys_,             // system of ODE
						    &t,                   // from t
						    B_.step_,            // to t <= step
						    &B_.IntegrationStep_, // integration step size
						    S_.y_);              // neuronal state
	 
	  

	  // Need to calculate w_V
	  
	  const double& V         = S_.y_[State_::V_M];
	  const double&  g_ex     = S_.y_[State_::G_EXC ];
	  const double&  g_in     = S_.y_[State_::G_INH ];
	  const double&  g_ex_n   = S_.y_[State_::G_EXC_N];
	  const double&  g_inh_b  = S_.y_[State_::G_INH_B];
	  const double&  w  	  = S_.y_[State_::W];
	  // const double I_syn_exc_aux = g_ex_n * ( V - P_.E_ex );
	
	
	  
	
	  const double I_syn_exc = g_ex * (P_.E_ex -V);
	  const double I_syn_inh = g_in * (P_.E_in -V);
	  const double I_syn_exc_n = sgate() * g_ex_n * ( P_.E_ex -V );
      const double I_syn_inh_b = g_inh_b * (P_.E_inh_b-V);


	  // We pre-compute the argument of the exponential
	  const double exp_arg=(V - P_.V_th) / P_.Delta_T;
	  // If the argument is too large, we clip it.
	  const double I_syn = I_syn_exc +I_syn_inh + I_syn_exc_n + I_syn_inh_b;
	  const double I_spike =  P_.g_L*P_.Delta_T * std::exp(exp_arg);
	  //const double I_tot = - I_syn_exc - I_syn_inh - I_syn_exc_n + P_.I_e;
	  //std::cout<<"I_tot "<<I_tot<<std::endl;

	  const double wV = P_.I_e + I_syn -P_.g_L * (V-P_.E_L) + I_spike ;
	  const double D0 = P_.C_m / P_.g_L * wV ;
	  //const double dD0 = P_.C_m *(std::exp(exp_arg)-1);

	  // The loop continues here
	  if ( status != GSL_SUCCESS )
	    throw GSLSolverFailure(get_name(), status);

	  // check for unreasonable values; we allow V_M to explode
	  if ( S_.y_[State_::V_M] < -1e3 ||
	       S_.y_[State_::W  ] <    -1e6 || S_.y_[State_::W] > 1e6    )
	    throw NumericalInstability(get_name());

	  // spikes are handled inside the while-loop
	  // due to spike-driven adaptation
	  const double t1_i = t0_i + P_.Delta_T*(P_.V_peak_-vp)/(S_.y_[State_::V_M]-vp);
	  const double  wVup = wp + ((S_.y_[State_::W] - wp)/P_.Delta_T)*(t1_i - t0_i);
	  //const double Delta_T
	  if ( S_.r_ > 0 )
	    S_.y_[State_::V_M] = P_.V_reset_;
	  if (( S_.y_[State_::V_M] >= P_.V_peak_) and (vp<P_.V_peak_))
	    {
	      S_.y_[State_::V_M]  = P_.V_reset_;
	      
		  S_.y_[State_::W]   += P_.b; // spike-driven adaptation
		  
	      S_.r_               = V_.RefractoryCounts_;
		  

	      set_spiketime(Time::step(origin.get_steps() + lag + 1));
	      SpikeEvent se;
	      kernel().event_delivery_manager.send( *this, se, lag );
	    }
	}
	  const double& V         = S_.y_[State_::V_M];
	  const double&  g_ex     = S_.y_[State_::G_EXC ];
	  const double&  g_in     = S_.y_[State_::G_INH ];
	  const double&  g_ex_n   = S_.y_[State_::G_EXC_N];
	  const double&  g_inh_b   = S_.y_[State_::G_INH_B];

	  const double exp_arg=(V - P_.V_th) / P_.Delta_T;
	  const double I_syn_exc = g_ex * (P_.E_ex -V);
	  const double I_syn_inh = g_in * (P_.E_in -V);
	  const double I_syn_inh_b = g_inh_b * (P_.E_inh_b -V);
	  const double I_syn_exc_n = sgate() * g_ex_n * ( P_.E_ex -V );

	  const double&  w  	  = S_.y_[State_::W];
	  const double I_syn = I_syn_exc +I_syn_inh + I_syn_exc_n +I_syn_inh_b ;
	  const double I_spike =  P_.g_L*P_.Delta_T * std::exp(exp_arg);
	  
	  const double wV = P_.I_e + I_syn -P_.g_L * (V-P_.E_L) +I_spike ;
	  const double D0 = P_.C_m / P_.g_L * wV ;
	  
	 
/* 	  for (size_t, i=0; i<nvar; i++)
	  { 
		
		S_.y_[State_:: ] +=  B_.step_/2.0*(dv1[i]+dv2[i]);
		f[S::V_M  ] = B_.step_/2.0*(dv1[i]+dv2[i]) 
		
		S_.y_[State_:: V_M] +=  B_.step_/2.0*(dv1[i]+dv2[i]);
		f[S::V_M  ] = B_.step_/2.0*(dv1[i]+dv2[i]) 
		
	  }
	   */
	  if (( w > wV - D0/P_.tau_w) && (w < wV + D0/P_.tau_w) && (V<=P_.V_th) ) 
	  {
		S_.y_[State_::W] = wV - D0/P_.tau_w;
	  }
	  //else 
	  //{
	//	S_.y_[State_::W] = 0;
	 // }
	
	//if(S_.y_[State_::W]>0){
	// std::cout<<"W "<<S_.y_[State_::W]<<std::endl; 
	//}
	  
	  
	//}  
	
	  //const double sgate = P_.Mg_gate / ( 1.0 + P_.Mg_fac * std::exp( P_.Mg_slope * ( - S_.y_[State_::V_M] )));
		
      S_.y_[State_::DG_EXC] += B_.spike_exc_.get_value( lag ) * V_.g0_ex_ ;
      S_.y_[State_::DG_INH] += B_.spike_inh_.get_value(lag) * V_.g0_in_;
      S_.y_[State_::DG_EXC_N] += B_.spike_exc_n_.get_value( lag ) * V_.g0_ex_n;
      S_.y_[State_::DG_INH_B] += B_.spike_inh_b_.get_value( lag ) * V_.g0_inh_b;

	  //std::cout<<"V_.g0_ex_n "<<V_.g0_ex_n<<std::endl;
	  

      // set new input current
      B_.I_stim_ = B_.currents_.get_value(lag);

      // log state data
      B_.logger_.record_data(origin.get_steps() + lag);
    }

}



port mynest::aeif_mod2_new_nest::handles_test_event(SpikeEvent&, port receptor_type)

{
	if (receptor_type != 0)
   
		return receptor_type;
	else 
		return 0;
}








void mynest::aeif_mod2_new_nest::handle(SpikeEvent & e)
{
  assert(e.get_delay_steps() > 0);

  if(e.get_weight() > 0.0){
	  if (e.get_rport() == 1)
	  {
	  B_.spike_exc_.add_value(e.get_rel_delivery_steps(kernel().simulation_manager.get_slice_origin()),
			 e.get_weight() * e.get_multiplicity());
	  }
	  else if (e.get_rport() == 2)
	  {
		B_.spike_exc_n_.add_value(e.get_rel_delivery_steps(kernel().simulation_manager.get_slice_origin()),
			 e.get_weight() * e.get_multiplicity());
	  }
	  else 
	  {
		B_.spike_exc_.add_value(e.get_rel_delivery_steps(kernel().simulation_manager.get_slice_origin()),
			 e.get_weight() * e.get_multiplicity());
	  }
    //std::printf("Receiving spike. Weight: %f, multiplicity: %d\n", e.get_weight(), e.get_multiplicity());

  }
  else{
        B_.spike_inh_.add_value(e.get_rel_delivery_steps(kernel().simulation_manager.get_slice_origin()),
			 -e.get_weight() * e.get_multiplicity());  // keep conductances positive
        B_.spike_inh_b_.add_value(e.get_rel_delivery_steps(kernel().simulation_manager.get_slice_origin()),
			 -e.get_weight() * e.get_multiplicity());  // keep conductances positive
    }
        //else
        //{
        //B_.spike_inh_.add_value(e.get_rel_delivery_steps(kernel().simulation_manager.get_slice_origin()),
//			 -e.get_weight() * e.get_multiplicity());  // keep conductances positive
//	    }
    // std::printf("Receiving spike. Weight: %f, multiplicity: %x\n", e.get_weight(), e.get_multiplicity());


}

void mynest::aeif_mod2_new_nest::handle(CurrentEvent& e)
{
  assert(e.get_delay_steps() > 0);

  // add weighted current; HEP 2002-10-04
  B_.currents_.add_value(e.get_rel_delivery_steps(kernel().simulation_manager.get_slice_origin()),
			 e.get_weight()*e.get_current());
}

void mynest::aeif_mod2_new_nest::handle(DataLoggingRequest& e)
{
  B_.logger_.handle(e);
}

#endif //HAVE_GSL_1_11
