#!/bin/bash

# Instruction from: 
# https://nest-simulator.readthedocs.io/en/v2.18.0/installation/linux_install.html#standard-installation

download_dir="$HOME/downloads"
install_dir="$HOME/nest_2.18.0"

if [ ! -d ${download_dir} ]
then
  mkdir -p ${download_dir}
fi

if [ ! -d ${install_dir} ]
then
  mkdir -p ${install_dir}
fi

cd ${download_dir}

# Install dependencies

#sudo apt-get install -y \
#build-essential \
#cmake \
#cython \
#libgsl-dev \
#libltdl-dev \
#libncurses-dev \
#libreadline-dev \
#python-all-dev \
#python-numpy \
#python-scipy \
#python-matplotlib \
#python-nose \
#openmpi-bin \
#libopenmpi-dev
sudo apt-get install -y \
build-essential \
cmake \
cython \
libgsl-dev \
libltdl-dev \
libncurses-dev \
libreadline-dev \
python-all-dev \
python-numpy \
python-scipy \
python-matplotlib \
python-nose \
openmpi-bin \
libopenmpi-dev

# download tar ball
wget https://github.com/nest/nest-simulator/archive/v2.18.0.tar.gz

# untar
tar -xzvf v2.18.0.tar.gz

# remove tar
rm v2.18.0.tar.gz

# Create build directory
mkdir nest-simulator-2.18.0-build
cd nest-simulator-2.18.0-build

cmake -DCMAKE_INSTALL_PREFIX:PATH="${install_dir}" "${download_dir}/nest-simulator-2.18.0"

# Compile and install nest
make
make install
make installcheck

# Add variables to bashrc
echo "source ${install_dir}/bin/nest_vars.sh" >> $HOME/.bashrc
source $HOME/.bashrc

# Clean up
cd ${download_dir}
rm -r nest-simulator-2.18.0
rm -r nest-simulator-2.18.0-build
