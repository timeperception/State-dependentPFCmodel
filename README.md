# State-dependentPFCmodel


## Installation
For conda installation, run:
```
conda create -n statePFC python==3.8 pip
conda activate statePFC
pip install -r requirements.txt
```
Note that this does not install the nest package. However, this is also not required for plot generation. For the simulations, we used nest 2.18.0. For installation, check  [NEST Simulator](https://nest-simulator.readthedocs.io/en/v2.18.0/installation/linux_install.html#standard-installation). For installation on Mac and cluster, you can also run the files 

```
install_nest_2.18.sh
install_nest_2.18_mac.sh

```
within the folder "/gitlab/State-dependentPFCmodel/NEST_simpadex Neuron model".

## Installing MyModule: 

Before running the NEST simulations, you have to install the mymodule saved in the folder: "NEST_simpadex Neuron model". Within this folder you can run 

```
compile_and_install_MyModule.sh

```
which should install the simpAdEx neuron model (simplified adaptive exponential integrate-and-fire neuron model) according to Hass, Hertaeg and Durstewitz (2016). You can test whether the compilation worked by the file

```
test_mymodule.py

```
An instruction can also be found here: https://nest-extension-module.readthedocs.io/en/latest/extension_modules.html



## Running simulations
In order to run simulations, NEST and MyModule must be installed. To run the original simulation presented in the paper, run: 

```
create_gridsearch_param_dframe.py

```
to create the save_folder. All necessary parameters to run the simulations (also for the ablation studies) can be found in 
```
load_exp_params.py

```
Execute this file and start the simulation by 
```
python Simulation_run_with_Ray.py --gsearch_exp='Original_train' --use_ray 
```
Ray uses all CPUs and allows the simulations to run in parallel. 


## Analysis 

In order to run some analysis, presented in the paper, use the two jupyter notebooks 

```
TrainReadoutWeights_CalculateEstimatedTime_WebersLaw.ipynb
AnalysisOnReadoutWeights.ipynb
```
Within the folder ResultsOfSimulation_called_Original.zip you'll find the results of one simulation which was stated as "original" within the paper, namely the states extracted from the spiketrains from the training data and from the test data. 
With the first jupyter notebook, you can calculate:
- the readout weights 
- outputs with the teststates
- the estimated times
- the standard deviations with a linear, square-root and a piecewise linear fit
- Weber fraction 

With the second jupyter notebook, the following results can be reproduced: 
- thresholding and sorting of readout weights
- plotting these weights
- thresholding and sorting the states and plotting them 
- calculate the averaged firingrates within a pool (normalized y-axis) 
- calculate the averaged firingrates within a pool (normalized x and y-axis) 



## Name
Time perception within the state-dependent PFC model 

## Abstract 

Coordinated movements, speech and other actions are impossible without precise timing. Realistic computational models of interval timing in the mammalian brain are expected to provide key insights into the underlying mechanisms of timing. Existing computational models of time perception have only been partially replicating experimental observations, such as the linear increase of time, the dopaminergic modulation of this increase, and the scalar property, i.e., the linear increase of the standard deviation of temporal estimates. In this work, we incorporate the state-dependent computational model, which encodes time in the dynamic evolution of network states without the need for a specific network structure into a biologically plausible prefrontal cortex (PFC) model based on in vivo and in vitro recordings of rodents. Specifically, we stimulated 1000 neurons in the beginning and in the end of a range of different time intervals, extracted states of neurons and trained the readout layer based on these states using least squares to predict the respective inter stimulus interval. We show that the naturally occurring heterogeneity in cellular and synaptic parameters in the PFC is sufficient to encode time over several hundreds of milliseconds. The readout faithfully represents the duration between two stimuli applied to the superficial layers of the network, thus fulfilling the requirement of a linear encoding of time. A simulated activation of the D2 dopamine receptor leads to an overestimation and an inactivation to an underestimation of time, in line with experimental results. Furthermore, we show that the scalar property holds true for intervals of several hundred milliseconds, and provide a mechanistic explanation for the origin of the scalar property as well as its deviations. We conclude that this model can represent durations up to 750 ms in a biophysically plausible setting, compatible with experimental findings in this regime.

## Citation 

For citations use the following paper: 
@article{ravichandran2022testing,
  title={Testing the state-dependent model of time perception against experimental evidence},
  author={Ravichandran-Schmidt, Pirathitha and Hass, Joachim},
  journal={bioRxiv},
  pages={2021--12},
  year={2022},
  publisher={Cold Spring Harbor Laboratory}
}

Ravichandran-Schmidt, P., & Hass, J. (2022). Testing the state-dependent model of time perception against experimental evidence. bioRxiv, 2021-12.

If you have any questions, please use the email adress within the paper. 