import socket
import os
import pandas as pd
import argparse
import ray
import numpy as np
import random
import time
import yaml
from tqdm import tqdm
import functions_simulation as funcsim

import itertools
hostname = socket.gethostname()

if "pira" in hostname.lower():
    base_folder = "/Users/pirathitharavichandran/Documents/pot_cond_of_special_simulations/"
    default_num_cpus = 4
elif "uc" in hostname.lower():
    base_folder = "/pfs/work7/workspace/scratch/hd_ta181-state_space/results_mai_2022/"
    default_num_cpus = None
else:
    default_num_cpus=20
    base_folder = '/zi-flstorage/Pirathitha.Ravichandran/'


parser = argparse.ArgumentParser()
parser.add_argument('--num_cpus', type=int, default=default_num_cpus)
parser.add_argument('--folder', type=str, default=base_folder)
parser.add_argument('--gsearch_exp', type=str, default='')
parser.add_argument('--use_ray', default=True, action='store_true')
parser.add_argument('--no-use_ray', dest='use_ray', action='store_false')

args = parser.parse_args()
folder = args.folder
num_cpus = args.num_cpus
use_ray = args.use_ray
gsearch_exp = args.gsearch_exp
assert gsearch_exp != ''

print('Running with arguments:')
print(vars(args))

'''
if use_ray:
if 'ip_head' in os.environ:
    ray.init(address=os.environ["ip_head"])
else:
    ray.init(num_cpus=num_cpus)
'''

@ray.remote
def Simulate_Network_ray(*args, **kwargs):
    return Simulate_Network_sim(*args, **kwargs)


def Simulate_Network_sim(
    GABAB_adj_factor,
    syn_ex_fac, syn_in_fac, percent_DA,
    fac_GABAA, fac_GABAB, adj_factor,adj_factor_layer5,  trial_wert,
    interval, seed, folder_save, select, firing_poisson,
    weight_poisson , syn_factor, fac_NMDA, sim_time,perturbation_current, num_poisson,
    number_input, inhibi_factor, E_inh_b_factor, gmax_in_factor, tau_ingabab_factor, tau_offgabab_factor, N_cores=1, STP = True, perturbation=False,
    weights_within_pools=False, fixed_seed = False, layer23exc=False, layer5exc=False, params_folder=''):


    global stimulus3
    import nest
    import params_loader

    assert len(params_folder)
    param = params_loader.get_params(params_folder)
    # print(params_folder)

    try:
        nest.Install('mymodule')
    except:
        print("Loading mymodule failed - already loaded?")

    nest.ResetKernel()
    print('Trialwert = ' + str(trial_wert))
    print('Interval_time = ' + str(interval))

    nest.SetKernelStatus({'local_num_threads': N_cores, 'resolution': 0.1})
    NTypes = param.SimPar['SimPar']['NTypes']
    print(NTypes)

    np.random.seed(seed)
    random.seed(seed)

    seeds = np.full(shape=N_cores, fill_value=seed)
    nest.SetStatus([0], {'rng_seeds': tuple(seeds)})
    nest.SetKernelStatus({'grng_seed': seed})
    ALL_RIGHT = True  # Avoids running the simulation if there is any problem

    real_simtime = sim_time  # min_delay*(wished_simtime/min_delay + 1)

    error_tol = 1e-4 # Default is 1e-6 but might be too slow if many spikes or give some problems with the solver. 1e-4 or 1e-3 should be enough
    print("error tolerance: ", error_tol)
    T_skip = 500.0

    initial_time = time.time()  # Used to calculate the simulation time

    t_ref = 5.0

    N_neurons = len(param.NeuPar)
    print(N_neurons)
    ConPar = param.SimPar['SimPar']['ConPar']
    print('fixed_stimulus: ' + str(select))

    stimulus = nest.Create('step_current_generator')
    print(interval)
    nest.SetStatus(stimulus,
                   {'amplitude_times': [1500.0, 1510.0], 'amplitude_values': [float(select), 0.0]})
    stimulus2 = nest.Create('step_current_generator')
    nest.SetStatus(stimulus2, {'amplitude_times': [1510.0 + interval, 1510.0 + interval + 10.0],
                               'amplitude_values': [float(select), 0.0]})


    #################################################
    #Events_current[Events_current==250] =np.int(adj_factor*250)
    ####include factor for other tests
    ###################################################

    Events_current = param.EvtMtx[0]
    Events_current = np.array(Events_current)  # * adj_factor

    #### only change onto exc units

    if layer5exc == True: 
        Events_current[575:955] = np.int(adj_factor_layer5*250)
    if layer23exc == True:
        Events_current[0:470] = np.int(adj_factor * 250)
    else:
        Events_current[Events_current == 250] = np.int(adj_factor * 250)

    Events_current[Events_current == 200] = np.int( inhibi_factor* 200)

    #print(adj_factor, Events_current)
    neurons = []  # Will store neurons' ID number beginning in 1
    for i, ParSet in enumerate(param.NeuPar):
        # Some synaptic parameter are set within the neuron
        [gmax_in, tau_in_on, tau_in_off, E_in_rev] = param.STypPar['GABA'][0:4]
        [gmax_n, tau_on_n, tau_off_n, E_rev_n, Mg_gate, Mg_fac, Mg_slope, Mg_half] = param.STypPar['NMDA']
        [gmax, tau_on, tau_off, E_rev] = param.STypPar['AMPA'][0:4]
        V_thres = float(ParSet[9]) - 		percent_DA *(ParSet[9]*0.037) 
        V_peak = float(ParSet[4])  - 	percent_DA *(ParSet[4]*0.041) 
        #print(V_peak, V_thres)
        if V_peak <= V_thres:
            #print('V_peak = ' +str(V_peak))
            V_peak = V_thres + 1.0

        E_inh_b = E_inh_b_factor # float(-90)
        g_max_gabab =  float(gmax_in / gmax_in_factor)   #float(gmax_in / 5.0)
        tau_in_gabab = tau_ingabab_factor # float(200) #float(50) #float(100)
        tau_off_gabab = tau_offgabab_factor #float(400) #  float(200)


        neurons.append(nest.Create('aeif_mod2_new_nest', 1, {
            'w': 			float(param.V0[1][i]),
            'V_m': 			float(param.V0[0][i]),
            'C_m': 			float(ParSet[0])    +   percent_DA*(ParSet[0]*0.012),
            'g_L': 			float(ParSet[1])   -    percent_DA*(ParSet[1]*0.142),
            'E_L': 			float(ParSet[2])    +   percent_DA*(ParSet[2]*0.066),
            'Delta_T': 		float(ParSet[3])+	 percent_DA*(ParSet[3]*0.278) ,
            'V_peak': 		V_peak,
            'tau_w': 		float(ParSet[5])-	percent_DA*(ParSet[5]*0.142),
            'a': 			float(ParSet[6]),
            'b': 			float(ParSet[7]) +	percent_DA*(ParSet[7]*0.098),
            'V_reset': 		float(ParSet[8]) -	percent_DA*(ParSet[8]*0.05) ,
            'V_th': 		V_thres,
            'I_e': 			GABAB_adj_factor * float(Events_current[i]), #float(param.EvtMtx[0][i]),  # -50.0),
            't_ref': 		t_ref,
            'gsl_error_tol': error_tol,
            'gpeak_I': 		 gmax_in*fac_GABAA      - percent_DA*gmax_in*fac_GABAA*0.5,
            'gpeak_Inhb' : 	 g_max_gabab*fac_GABAB - percent_DA*g_max_gabab*fac_GABAB*0.5,
            'tau_syn_in_on': 	tau_in_on,
            'tau_syn_in_off':	tau_in_off,
            'E_in': 			E_in_rev,
            'E_inh_b':			E_inh_b,
            'Mg_gate': 			Mg_gate,
            'Mg_fac': 			Mg_fac,
            'Mg_slope': 		Mg_slope,
            'Mg_half': 			Mg_half,
            'gpeak_NMDA': 		gmax_n*fac_NMDA - percent_DA*fac_NMDA*gmax_n*0.2,
            'tau_syn_NMDA_on': 	tau_on_n,
            'tau_syn_NMDA_off': tau_off_n,
            'gpeak_AMPA': 		gmax,
            'tau_syn_AMPA_on': 	tau_on,
            'tau_syn_AMPA_off': tau_off,
            'tau_syn_Inhb_on': 	tau_in_gabab,
            'tau_syn_Inhb_off': tau_off_gabab,
            'E_ex': 			E_rev,
            'I_ref': 			ParSet[10],
            'V_dep': 			ParSet[11]
        })[0])
    

    print('GABAB ist ' + str(fac_GABAB))
    poisson = nest.Create('poisson_generator', num_poisson)
    nest.SetStatus(poisson, {'rate':  firing_poisson})
    parrot = nest.Create('parrot_neuron', num_poisson)
    nest.Connect(poisson, parrot)
    
    nest.Connect(stimulus, neurons)		#[0:575])  # ,conn)
    nest.Connect(stimulus2, neurons)	#[0:575])  # ,conn)
    
    # target_firing = 1000.0
    '''
    SPMtx_1 = np.reshape(param.SPMtx[0], (N_neurons, N_neurons + 2))
    SPMtx_2 = np.reshape(param.SPMtx[1], (N_neurons, N_neurons + 2))

    SPM = [SPMtx_1, SPMtx_2]
    syn_weights = np.zeros((len(SPM[0][:, 0]), len(SPM[0][0, :]), 2))
    tau_recs = np.zeros((len(SPM[0][:, 0]), len(SPM[0][0, :]),2))
    tau_facs = np.zeros((len(SPM[0][:, 0]), len(SPM[0][0, :]),2))
    syn_k_2 = []
    if weights_within_pools == True:
        syn_weights_alltoall = np.load(params_folder + '/syn_weights_alltoall.npy')  # param.syn_weights_alltoall
    for k in np.arange(0, 2):
        for post in np.arange(0, len(SPM[k][:, 0])):
            for pre in np.arange(0, len(SPM[k][0, :])):
                syn_ID = SPM[k][post, pre]

                if syn_ID != 0:
                    syn_type = param.SynPar[syn_ID - 1][0]
                    u_se = param.SynPar[syn_ID - 1][1]
                    tau_rec = param.SynPar[syn_ID - 1][2]
                    tau_fac = param.SynPar[syn_ID - 1][3]
                    syn_weight = (param.SynPar[syn_ID - 1][4])
                    syn_delay = param.SynPar[syn_ID - 1][5]
                    #print(syn_delay)
                    if syn_delay<=0.1:
                        syn_delay=0.1
                    # syn_proba_fail = SynPar[syn_ID-1][6]
                    if k == 1:
                        syn_k_2.append(syn_type)
                        if syn_type == 1.0:
                            syn_weight = 0.0
                    if syn_type == 2.0:
                        syn_weight = - syn_weight * gmax_in * syn_in_fac
                        receptor_type =1
                    elif syn_type == 1.0:
                        syn_weight = syn_weight * 	gmax * syn_ex_fac
                        receptor_type = 1
                    elif syn_type == 3.0:
                        syn_weight = syn_weight *	gmax_n  * syn_ex_fac
                        receptor_type = 2
                    syn_weights[pre, post, k] = syn_weight
                    tau_recs[pre, post, k] = tau_rec
                    tau_facs[pre, post, k] = tau_fac
                    if STP == True:
                        #syn_weights_alltoall = param.syn_weights_alltoall
                        #syn_weight = syn_weights_alltoall[pre, post, k]
                        syn_dict = {"model": "tsodyks_synapse", "weight": syn_weight*syn_factor, "delay": syn_delay,
                                'receptor_type': receptor_type, 'U': u_se,
                                'tau_rec': tau_rec,
                                'tau_fac': tau_fac,
                                'x': 1.0,
                                'u': u_se}
                    elif weights_within_pools==True:
                        # print('Korrekt')

                        syn_weight = syn_weights_alltoall[pre, post, k]
                        syn_dict = {"model": "tsodyks_synapse", "weight": syn_weight*syn_factor, "delay": syn_delay,
                                'receptor_type': receptor_type, 'U': u_se,
                                'tau_rec': tau_rec,
                                'tau_fac': tau_fac,
                                'x': 1.0,
                                'u': u_se}
                        #print(syn_weight)
                        #print('syn_params are set')
                        #print(syn_weight, tau_fac, tau_rec)
                    else:
                        #print('noSTP')
                        #syn_weight = syn_factor*1.0
                        syn_dict = {"model": "static_synapse", "weight": syn_weight*syn_factor , "delay": syn_delay,
                                'receptor_type':receptor_type}
                    nest.Connect([neurons[pre]], [neurons[post]], syn_spec=syn_dict)


            #print('synparams are set' + str(post)+str(syn_weight))
    '''


    
    SPM = np.stack(param.SPMtx, axis=0).reshape(-1, N_neurons, N_neurons + 2)
    synIDs = SPM.reshape(-1)
    syn_weights = param.SynPar[synIDs - 1, 4]
    syn_types = param.SynPar[synIDs - 1, 0]
    u_ses = param.SynPar[synIDs - 1, 1]
    tau_recs = param.SynPar[synIDs - 1, 2]
    tau_facs = param.SynPar[synIDs - 1, 3]
    syn_delays = param.SynPar[synIDs - 1, 5]
    receptor_types = np.copy(syn_types)
    receptor_types[syn_types == 1] = 1
    receptor_types[syn_types == 2] = 1
    receptor_types[syn_types == 3] = 2

    syn_delays[syn_delays <= 0.1] = 0.1

    syn_types = syn_types.reshape((SPM.shape[0], -1))
    syn_weights = syn_weights.reshape((SPM.shape[0], -1))
    syn_weights[1, syn_types[1] == 1] = 0.0
    syn_types = syn_types.reshape(-1)
    syn_weights = syn_weights.reshape(-1)
    syn_weights[syn_types == 1] *= gmax * syn_ex_fac
    syn_weights[syn_types == 2] *= -1.0 * gmax_in * syn_in_fac
    syn_weights[syn_types == 3] *= gmax_n * syn_ex_fac
    
    _, posts, pres = np.unravel_index(np.arange(len(syn_weights)), shape=SPM.shape)

    keep_mask = np.logical_and(synIDs != 0, syn_weights != 0)
    synIDs = synIDs[keep_mask]
    syn_weights = syn_weights[keep_mask] * syn_factor
    syn_types = syn_types[keep_mask]
    u_ses = u_ses[keep_mask]
    tau_recs = tau_recs[keep_mask]
    tau_facs = tau_facs[keep_mask]
    syn_delays = syn_delays[keep_mask]
    receptor_types = receptor_types[keep_mask].astype(np.int)
    neurons_np = np.array(neurons)
    posts = posts[keep_mask]
    pres = pres[keep_mask]
    posts = list(neurons_np[posts])
    pres = list(neurons_np[pres])
    
    syn_dict = {
        "model": "tsodyks_synapse", 
        "weight": syn_weights, 
        "delay": syn_delays,
        'receptor_type': receptor_types,
        'U': u_ses,
        'tau_rec': tau_recs,
        'tau_fac': tau_facs,
        'x': 1.0,
        'u': u_ses}
    
    
    # '''
    print('starting connect')
    nest.Connect(pres, posts, syn_spec=syn_dict, conn_spec='one_to_one')
    print('connect done')
    # '''


    #### set_fix_seed to have the same connectivities between the trials for poisson noise
    if fixed_seed == True:
        st0 = np.random.get_state()
        #set seed to 0
        np.random.seed(0)
        random.seed(0)

    # pick connections
    syn_spec_poisson = {"model": "static_synapse", "weight": weight_poisson}
    #num_connections = N_neurons*int(num_poisson/firing_poisson)
    num_connections = N_neurons*int(number_input)
    idcs_poisson = np.random.randint(low=0, high=num_poisson, size=num_connections)
    idcs_neurons = np.random.randint(low=0, high=N_neurons, size=num_connections)
    for idx_poisson, idx_neuron in zip(idcs_poisson, idcs_neurons):
        nest.Connect([parrot[idx_poisson]], [neurons[idx_neuron]], syn_spec=syn_spec_poisson)

    if fixed_seed == True:
        np.random.set_state(st0) ## set the state back to what it was originally

    spiker = 		nest.Create('spike_detector')
    spiker_parrot = nest.Create('spike_detector')
    print('set spiker')
    multimeter = []
    ViewList = [neurons[0:1]] # all neurons


    for n in ViewList[0]:
        #multimeter.append((n,nest.Create('multimeter', params = {'interval':0.1, 'withtime': True,'record_from': ['V_m', 'w', 'g_in', 'g_ex_n', 'g_ex','g_inh_b']})))
        multimeter.append((n, nest.Create('multimeter', params={'interval': 0.1, 'withtime': True,
                                            'record_from': ['V_m', 'g_in', 'g_inh_b', 'g_ex', 'g_ex_n']})))
        nest.Connect(multimeter[len(multimeter)-1][1], [n])

    nest.Connect(neurons, spiker)  # All is connected to spiker so it will register the spikes
    nest.Connect(poisson, spiker_parrot)
    print('start simulation')
    if ALL_RIGHT:
        #print(nest.PrintNetwork())
        mid = time.time()
        nest.Simulate(real_simtime)
        final = time.time()
        s = "*******************************************************"
        print(s)
        print("network building time: ", mid - initial_time)
        print("simulation time: ", final - mid)
        print(len(neurons))

    mult_events = []
    for i in multimeter:
        mult_events.append((i[0], nest.GetStatus(i[1])[0]['events']))
    np.save(folder_save+ '/'+ str(GABAB_adj_factor)+str(trial_wert)+str(interval)+'mult_events.npy', mult_events)

    ##################  Saving important data.##################
    spt = nest.GetStatus(spiker)[0]['events']['times'].tolist()

    spikes_show_parrot = nest.GetStatus(spiker_parrot)[0]['events']
    sender_poisson = 	spikes_show_parrot['senders'].tolist()
    times_poisson = 	spikes_show_parrot['times'].tolist()

    spikes_show  =   nest.GetStatus(spiker)[0]['events']
    sender_spike = 	 np.array(spikes_show['senders'].tolist())
    times_spike  = 	 np.array(spikes_show['times'].tolist())

    location_after_500ms = 	np.where(times_spike > T_skip)
    spiketrains_python   = 	[[] for i in np.arange(0, len(neurons) - 2)]
    times_spike_reduced  =  times_spike[location_after_500ms]
    sender_spike_reduced =  sender_spike[location_after_500ms]

    for i in np.arange(0, len(neurons) - 2):
        a = np.where(sender_spike_reduced == (i))
        spiketrains_python[i].append(np.take(times_spike_reduced, a[0]))


    np.save(os.path.join(folder_save,str(trial_wert) + str(interval)+ str(adj_factor)+ str(adj_factor_layer5)+ str(fac_GABAB)+str(fac_GABAA)+ str(percent_DA)+str(syn_ex_fac) +
                         str(syn_in_fac)+str(syn_factor)+str(GABAB_adj_factor) + str(weight_poisson) +
                         #str(perturbation_current) +
                         'spiketrains.npy'), spiketrains_python)
    np.save(os.path.join(folder_save,str(trial_wert)+ str(interval)+ str(adj_factor) +  str(adj_factor_layer5)+ str(fac_GABAB)+str(fac_GABAA)+ str(percent_DA)+str(syn_ex_fac) +
                         str(syn_in_fac)+str(syn_factor)+ str(GABAB_adj_factor) +
                         #str(perturbation_current) +
                         'sender_spike.npy'), sender_spike)
    np.save(os.path.join(folder_save,  str(trial_wert) + str(interval)+ str(adj_factor)+ str(adj_factor_layer5)+  str(fac_GABAB)+str(fac_GABAA)+ str(percent_DA)+str(syn_ex_fac) +
                         str(syn_in_fac)+str(syn_factor)+str(GABAB_adj_factor) +
                         #str(perturbation_current) +
                         'times_spike.npy'), times_spike)
    np.save(os.path.join(folder_save,str(trial_wert)+ str(interval)+ str(adj_factor) +  str(adj_factor_layer5)+  str(fac_GABAB)+str(fac_GABAA) +  str(percent_DA)+str(syn_ex_fac) +
                         str(syn_in_fac)+str(syn_factor)+str(GABAB_adj_factor) + 'sender_poisson.npy'), sender_poisson)
    np.save(os.path.join(folder_save,  str(trial_wert) + str(interval)+ str(adj_factor)+ str(adj_factor_layer5)+ str(fac_GABAB)+str(fac_GABAA) + str(percent_DA)+str(syn_ex_fac) +
                         str(syn_in_fac) + str(syn_factor)+ str(GABAB_adj_factor) + 'times_poisson.npy'), times_poisson)


    np.save(os.path.join(folder_save, 'syn_weights.npy'), syn_weights)
    np.save(os.path.join(folder_save, 'tau_recs.npy'), tau_recs)
    np.save(os.path.join(folder_save, 'tau_facs.npy'), tau_facs)



print(gsearch_exp)
dframe_path = os.path.join(base_folder, gsearch_exp, gsearch_exp + '_param_dframe.pkl')
dframeout_path = os.path.join(base_folder, gsearch_exp, gsearch_exp + '_results_dframe.pkl')
param_dframe = pd.read_pickle(dframe_path)
out_path = os.path.join(base_folder, gsearch_exp, 'params.yaml')

with open(out_path, 'r') as stream:
    params_loaded = yaml.safe_load(stream)

interval_start = params_loaded['interval_start']
interval_end = params_loaded['interval_end']
interval_step = params_loaded['interval_step']
intervals = np.arange(interval_start, interval_end , interval_step)

# intervals = np.load(base_folder + gsearch_exp + 'list_intervals.npy')

trial_start = params_loaded['trial_start']
trial_end = params_loaded['trial_end']
trial_step = params_loaded['trial_step']
trials = np.arange(trial_start, trial_end, trial_step)

GABAB_adj_factor_start = params_loaded['GABAB_adj_factor_start']
GABAB_adj_factor_end = params_loaded['GABAB_adj_factor_end']
GABAB_adj_factor_step = params_loaded['GABAB_adj_factor_step']
GABAB_adj_factors = np.arange(GABAB_adj_factor_start, GABAB_adj_factor_end, GABAB_adj_factor_step)

syn_factor_start = params_loaded['syn_factor_start']
syn_factor_end = params_loaded['syn_factor_end']
syn_factor_step = params_loaded['syn_factor_step']
syn_factors = np.arange(syn_factor_start, syn_factor_end, syn_factor_step)

syn_ex_fac_start = params_loaded['syn_ex_fac_start']
syn_ex_fac_end = params_loaded['syn_ex_fac_end']
syn_ex_fac_step = params_loaded['syn_ex_fac_step']
syn_ex_factors = np.arange(syn_ex_fac_start, syn_ex_fac_end, syn_ex_fac_step)

syn_in_fac_start = params_loaded['syn_in_fac_start']
syn_in_fac_end = params_loaded['syn_in_fac_end']
syn_in_fac_step = params_loaded['syn_in_fac_step']
syn_in_factors = np.arange(syn_in_fac_start, syn_in_fac_end, syn_in_fac_step)

factor_start = params_loaded['factor_start']
factor_end = params_loaded['factor_end']
factor_step = params_loaded['factor_step']
adj_factors = np.arange(factor_start, factor_end, factor_step)

adj_layer5_start = params_loaded['adj_layer5_start']
adj_layer5_step = params_loaded['adj_layer5_step']
adj_layer5_end= params_loaded['adj_layer5_end']
adj_layer5_factors = np.arange(adj_layer5_start, adj_layer5_end, adj_layer5_step)


fac_GABAB = params_loaded['fac_GABAB']
fac_NMDA = params_loaded['fac_NMDA']
params_folder = params_loaded['params_folder']
weight_poissons	 = params_loaded['weight_poisson']
num_poisson	 = params_loaded['num_poisson']
sim_time = params_loaded['sim_time']
perturbation_currents = params_loaded['perturbation_currents']
##### flags :
STP_flag = params_loaded['STP']
all_to_all_weights_flag = params_loaded['weights_within_pools']
layer23exc_flag = params_loaded['layer23exc_flag']
layer5exc_flag = params_loaded['layer5exc_flag']
perturbation_flag = params_loaded['perturbation_flag']
number_input = params_loaded['number_input']
inhibi_factor = params_loaded['inhibi_factor']

E_inh_b_factor = params_loaded['E_inh_b_factor']
gmax_in_factor = params_loaded['gmax_in_factor']
tau_ingabab_factor = params_loaded['tau_ingabab_factor']
tau_offgabab_factor = params_loaded['tau_offgabab_factor']



a = time.time()
print(params_folder)
futures = []
print(weight_poissons)

for file_id in np.arange(len(param_dframe)):
    firing_poisson = param_dframe['firing_poisson'][file_id]
    funcsim.update_param_dframe(param_dframe)
    path = param_dframe['path'][file_id]
    print(path)
    select = param_dframe['select'][file_id]
    # weight_poisson = param_dframe['weight_poisson'][file_id]
    fac_GABAA = param_dframe['fac_GABAA'][file_id]
    percent_DA = param_dframe['percent'][file_id]
    folder_save = path
    #######################################################
    # fac_GABAB =  params_loaded['fac_GABAB']  ## to guarantee 1/5 of weights as defined above
    # fac_GABAB = fac_GABAA
    #print(path)
    # weight_poissons = weight_poissons[file_id]
    # print(firing_poisson, weight_poisson)
    all_parameter_combinations = list(itertools.product(
        intervals, trials, syn_factors, GABAB_adj_factors, syn_ex_factors, syn_in_factors,
        adj_factors, adj_layer5_factors, weight_poissons, perturbation_currents))
    if not use_ray:
        all_parameter_combinations = tqdm(all_parameter_combinations)
    for cur_params in all_parameter_combinations:
        interval, trial_wert, syn_factor, GABAB_adj_factor, syn_ex_fac, syn_in_fac, \
        adj_factor, adj_factor_layer5, weight_poisson, perturbation_current = cur_params
        # trial_wert = trial_wert
        print(percent_DA, file_id, trial_wert)
        seed = trial_wert
        sim_args = (
        np.round(GABAB_adj_factor, 1), np.round(syn_ex_fac, 1), np.round(syn_in_fac, 1), percent_DA, fac_GABAA,
        fac_GABAB,
        np.round(adj_factor, 2), np.round(adj_factor_layer5, 2),
        trial_wert, interval, seed, folder_save,
        select, firing_poisson, weight_poisson, np.round(syn_factor, 2), fac_NMDA, sim_time, perturbation_current,num_poisson,number_input,inhibi_factor, 
        E_inh_b_factor, gmax_in_factor, tau_ingabab_factor, tau_offgabab_factor,    1)
        sim_kwargs = dict(
            STP=STP_flag,
            perturbation=perturbation_flag,
            weights_within_pools=all_to_all_weights_flag,
            fixed_seed=False,
            layer23exc=layer23exc_flag,
            layer5exc=layer5exc_flag,
            params_folder=params_folder
        )
        if use_ray:
            print('Starting Simulation')
            futures.append(Simulate_Network_ray.remote(*sim_args, **sim_kwargs))
        else:
            Simulate_Network_sim(*sim_args, **sim_kwargs)

    print('Done sending jobs')
    if use_ray:
        ready_ids = []
        remaining_ids = futures
    for i in tqdm(range(len(futures))):
        new_ready_ids, remaining_ids = ray.wait(remaining_ids, num_returns=1)
        ready_ids.append(new_ready_ids)
    _ = ray.get(futures)
    b = time.time()
    #print("Simulation with %d cores took: %.1f seconds" % (num_cpus, b - a))
